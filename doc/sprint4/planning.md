RCOMP 2018-2019 Project - Sprint 4 planning
===========================================
### Sprint master: 1170508 ###
(This file is to be created/edited by the sprint master only)
# 1. Sprint's backlog #

- OSPF based dynamic routing
- Adding a second server to each DMZ network to run the HTTP service.
- Configuring DNS servers to establish a DNS domains tree.
- Enforcing NAT (Network Address Translation).
- Establishing traffic access policies (static firewall) on routers.

# 2. Technical decisions and coordination #

## OSPF area ids

- Backbone - id 0
- Building A - id 1
- Building B - id 2
- Building C - id 3
- Building D - id 4
- Building E - id 5
- Building F - id 6

## DNS

### Domain
- rcomp18-19-dd-g1
### SubDomains
- (edificio B) building-B.rcomp18-19-dd-g1
- (edificio C) building-C.rcomp18-19-dd-g1
- (edificio D) building-D.rcomp18-19-dd-g1
- (edificio E) building-E.rcomp18-19-dd-g1
- (edificio F) building-F.rcomp18-19-dd-g1


### Servers

- Building B 172.18.42.5
- Building C 172.18.44.5
- Building D 172.18.32.5
- Building E 172.18.36.5
- Building F 172.18.46.5

# 3. Subtasks assignment #
(For each team member (sprint master included), the description of the assigned subtask in sprint 4)

*  1161071 - Update the campus.pkt layer three Packet Tracer simulation from the
previous sprint, to include the described features for this sprint for
building A.
* 1170785 -  Update the campus.pkt layer three Packet Tracer
simulation from the
previous sprint, to include the described features for this sprint for building B.
* 1170790 -  Update the campus.pkt layer three Packet Tracer simulation from the
previous sprint, to include the described features for this sprint for
building C.
Final integration of every members’ Packet Tracer simulations into a
single simulation.
* 1170508 - Update the campus.pkt layer three Packet Tracer simulation from the
previous sprint, to include the described features for this sprint for
building D.
* 1170505 - Update the campus.pkt layer three Packet Tracer simulation from the
previous sprint, to include the described features for this sprint for
building E.
* 1170803 - Update the campus.pkt layer three Packet Tracer simulation from the
previous sprint, to include the described features for this sprint for
building F.
