RCOMP 2018-2019 Project - Sprint 4 - Member 1170505 folder
===========================================
(This folder is to be created/edited by the team member 1170505 only)

The owner of this folder (member number 1170505) will commit here all the outcomes (results/artifacts/products) of his/her work during sprint 4. This may encompass any kind of standard file types.
