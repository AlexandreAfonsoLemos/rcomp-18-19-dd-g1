RCOMP 2018-2019 Project - Sprint 4 - Member 1170785 folder
===========================================
(This folder is to be created/edited by the team member 1170785 only)

The owner of this folder (member number 1170785) will commit here all the outcomes (results/artifacts/products) of his/her work during sprint 4. This may encompass any kind of standard file types.

#Explicação Suplementar

##FIREWALL

 * Static firewalls permit or deny individual packets based on static rules defined by the network administrator.
 * Criterions for packet matching (and consequent permit or deny) are relative to layer three and four properties: IP source and destination node addresses (IPv4/IPv6), payload type (e.g. ICMP, UDP, TCP), source and destination port numbers (for UDP and TCP payloads only), message types for ICMP payloads, etc.
 * Static firewalls react the same way to a given packet regardless of context, this makes them inadequate against DoS attacks. The problem is, nothing allows it to distinguish between a packet from a licit access to the service and packet from a DoS attack to the service. A different type of firewall is required to handle DoS attacks, they are called dynamic or stateful. Although they are not able to block some DoS attacks, static firewalls can do a lot in behalf of network security. On the top of the list of attacks that a static firewall can avoid are IP spoofing attacks.
	
 * In higher precedence first order, traffic access policies to be enforced are:
 
	**1.** Block all spoofing, as far as possible. Internal spoofing from local networks, the DMZ may be excluded. External spoofing in traffic incoming from the backbone network.
	
	**2.** All ICMP echo requests and echo replies are always allowed.
	
	**3.** All traffic to the DMZ is to be blocked, except for the DNS service and HTTP/HTTPS services to the corresponding servers. All traffic incoming from the DMZ is allowed.
	
	**4.** All traffic directed to the router itself (with a destination IPv4 node address belonging to the router) is to be blocked, except for the traffic required for the current features to work.
	
	**5.** Remaining traffic passing through the router should be allowed.

##IP SPOOFING

 * IP spoofing means forging the packet source IP address, this can be used for several illicit purposes, including concealing DoS attacks (Distributed DoS) to prevent them from being detected by dynamic firewalls. IP spoofing is also used to mislead static firewalls making them believe a packet is coming from an authorised source node address, and thus, allow access to some restricted services.
 * **Local networks’ addresses are known, but internet networks’ addresses are unknown.**
 	
	* Avoiding **internal spoofing**: any traffic going to the internet must have source addresses belonging to one of the known local networks.
	
	* Avoiding **external spoofing**: any traffic coming from the internet must have source addresses not belonging to any of the known local networks.
	
##DNS

 * Independent DNS domains are going to be established on each building.
 * The team member in charge of building A will create a DNS domain name matching the team’s repository name (rcomp-18-19-cc-gn). This is going to be the highest level domain, so it’s going to be used as if it was the DNS root domain.
 * Local DNS domains name: building-B.rcomp-18-19-cc-gn. (subdomain of the rcomp-18-19-cc-gn domain)

