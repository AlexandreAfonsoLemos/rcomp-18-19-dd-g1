RCOMP 2018-2019 Project - Sprint 1 - 1170505
===========================================
(This folder is to be created/edited by the team member 1170505 only)

The owner of this folder (member number 1170505) will commit here all the outcomes (results/artifacts/products) of his/her work during sprint 1. This may encompass any kind of standard file types.

# EDIFICIO E (80x30)
## Proposta de Esquema
### GROUND FLOOR

![Error](E0Floor.png)

| DIVISÃO | AREAS    | OUTLETS USADOS(CALCULADOS) | OUTLETS COLOCADOS | DISTANCIA AO CABO MAIS LONGO | CALCULOS
|---------|----------|----------------------------|-------------------|------------------------------|----------
| E0.1    | 48,22m^2 | 10                         | 16                | 38.92m                       | 155.68m
| E0.2    | 48,22m^2 | 10                         | 16                | 32.24m                       | 128.96m
| E0.3    | 48,22m^2 | 10                         | 16                | 25.29m                       | 101.16m
| E0.4    | 48,22m^2 | 10                         | 16                | 18.90m                       | 75.6m
| E0.5    | 48,22m^2 | 10                         | 16                | 6.19m                        | 24.76m
| E0.6    | 47,01m^2 | 10                         | 16                | 32.55m                       | 130.2m
| E0.7    | 47,01m^2 | 10                         | 16                | 24.13m                       | 96.52m
| E0.8    | 61,47m^2 | 14                         | 20                | 53.53m                       | 214.12m
| E0.9    | 48,22m^2 | 10                         | 16                | 65.43m                       | 261.72m
| E0.10   | 43,98m^2 | 10                         | 16                | 72.16m                       | 288.64m
| E0.11   | 43,98m^2 | 10                         | 16                | 77.67m                       | 310.68m
| WC      | 28,11m^2 |                                  
###INVENTARIO GROUND FLOOR
##### TOTAL OUTLETS | OUTLETS USADOS
-> 180 | 114
##### TOTAL CABO COBRE
-> 1788.04 m
##### TOTAL CALHA
-> 161.94 m
##### PATCH PANELS (24 PORTAS)
###### Cobre:
-> 5
###### Fibra:
-> 2
##### TELECOMUNICATION ENCLUSURES
-> 1
##### ACESS POINTS
-> 2
##### TOTAL CABO FIBRA GROUND FLOOR
-> 5.04 m (devida a ter em conta a redundância)
###### Descrição do andar:
  - O resto-de-chão é provido com um canal de cabos subterrâneos conectado a vala técnica exterior.
  - O acesso ao canal de cabos subterrâneos está disponível nos pontos marcados ao longo do plano.
  - A altura do teto neste piso é 5 metros, mas a área da esquerda dividida em salas tem um teto removível rebaixado, colocado a 2,5 metros do chão, cobrindo toda a área designada. Na área aberta da direita não e necessário existirem network outlets, assim como nas áreas comuns no lado esquerdo.

### INVENTARIO RACK E0.ICC
- 1 patch panel fibra

### INVENTARIO RACK E0.HCC
- 5 patch panels cobre
- 1 patch panels fibra

###### Cálculos e associações feitas através das medições.
- E0.1 = E0.2 = E0.3 = E0.4 = E0.5 = E0.9  
- E0.6 = E0.7
- E0.10 = E0.11

### FLOOR 1

![Error](E1Floor.png)

| DIVISÃO  | AREAS    | OUTLETS MINIMOS | OUTLETS ESTIMADOS | DISTANCIA AO CABO MAIS LONGO | CALCULOS
|----------|----------|-----------------|-------------------|------------------------------|----------
| E1.1     | 48,22m^2 | 10              | 16                | 25.72m                       |102.88
| E1.2     | 48,22m^2 | 10              | 16                | 18.93m                       |75.72
| E1.3     | 48,22m^2 | 10              | 16                | 12.24m                       |48.96
| E1.4     | 48,22m^2 | 10              | 16                | 18.79m                       |75.16
| E1.5     | 48,22m^2 | 10              | 16                | 25.42m                       |101.68
| E1.6     | 48,22m^2 | 10              | 16                | 32.13m                       |128.52
| E1.7     | 48,22m^2 | 10              | 16                | 25.35m                       |101.4
| E1.8     | 48,22m^2 | 10              | 16                | 19.29m                       |77.16
| E1.9     | 48,22m^2 | 10              | 16                | 12.27m                       |49.08
| E1.10    | 48,22m^2 | 10              | 16                | 18.58m                       |74.32
| E1.11    | 47,01m^2 | 10              | 16                | 13.71m                       |54.84
| E1.12    | 47,01m^2 | 10              | 16                | 6.39m                        |25.56
| E1.13    | 47,01m^2 | 10              | 16                | 34.81m                       |139.24
| E1.14    | 47,01m^2 | 10              | 16                | 25.80m                       |103.2
| E1.15    | 47,01m^2 | 10              | 16                | 16.90m                       |67.6
| E1.16    | 47,01m^2 | 10              | 16                | 8.15m                        |32.6
| E1.17    | 61,47m^2 | 14              | 20                | 41.94m                       |167.76
| E1.18    | 48,22m^2 | 10              | 16                | 27.98m                       |111.92
| E1.19    | 61,47m^2 | 14              | 20                | 21m                          |84
| E1.20    | 61,47m^2 | 14              | 20                | 7.5m                         |30
| E1.21    | 61,47m^2 | 14              | 20                | 19.97m                       |79.88
| E1.22    | 61,47m^2 | 14              | 20                | 28.39m                       |113.56
| E1.23    | 61,47m^2 | 14              | 20                | 36.76m                       |147.04
| E1.24    | 78,40m^2 | 16              | 20                | 46.88m                       |187.52
| WC Left  | 37,92m^2 |                 
| WC Right | 34,50m^2 |                 

###INVENTARIO FIRST FLOOR
##### TOTAL OUTLETS | OUTLETS USADOS
-> 412 | 270
##### TOTAL CABO COBRE
-> 2179.6 m
##### TOTAL CALHA
-> 342.5 m
##### PATCH PANELS (24 PORTAS)
###### Cobre:
-> 12
###### Fibra:
-> 3
##### TELECOMUNICATION ENCLUSURES | CP
-> 1 | 2
##### ACESS POINTS
-> 2
##### TOTAL CABO FIBRA FIRST FLOOR
 -> 144.64m (Devido as ligações aos CP e a ter em conta a redundância)
###### Descrição do andar:
  - A altura do teto deste piso é 3 metros mas existe um teto removível rebaixado, colocado a 2,5 metros do chão, cobrindo toda o andar.
  - Áreas comuns não necessitam de ter network outlets.

### INVENTARIO RACK E1.HCC
- 4 patch panel cobre
- 1 patch panel fibra

### INVENTARIO RACK E1.CP1
- 4 patch panels cobre
- 1 patch panels fibra

### INVENTARIO RACK E1.CP2
- 4 patch panels cobre
- 1 patch panels fibra

###### Cálculos e associações feitas através das medições.
- E1.1 = E1.2 = E1.3 = E1.4 = E1.5 = E1.6 = E1.7 = E1.8 = E1.9 = E1.10 = E1.18
- E1.11 = E1.12 = E1.13 = E1.14 = E1.15 = E1.16
- E1.17 = E1.19 = E1.20 = E1.21 = E1.22 = E1.23


###### Informação útil
- Regras de distribuição de cabos: não exceder os 90m, passar junto as paredes, dar prioridade aos caminhos pré existentes(azul claro) e tentar usar caminhos comuns para minimizar o uso de calhas.
- Regras de distribuição de outlets: assumindo cabos de rede com 5m por outlet, é necessário garantir que qualquer ponto da sala não se encontra a mais de 3 metros de um cabo.
- Foram utilizados cabos de cobre do tipo cat6 nos patch panels do horizontal crossconnect por uma questão de compatiblilidade com os dispositivos. Nos restantes paineis foi utilizado fibra para aumentar a transferência de dados e reduzir a interferência no sinal.
- O numero de access points utilizados em cada andar foi calculado com uma estimativa de alcance de 30m, porque assim é suficiente para cobrir o andar por completo.
- Para cada 10m^2 devem estar disponíveis no mínimo 2 outlets.
- As contas para o numero de outlets mínimos foram feitas seguindo estes números.
- Decidimos colocar mais outlets do que os mínimos porque com os mínimos não conseguimos assegurar o cumprimento da regra de distribuição de outlets.
- O edifício E possui também 2 horizontal cross connect em que cada faz a ligação dos respetivos outlets de cada andar ao intermediate cross connect atraves de cobre.
- Passagem de cabos entre pisos e underfloor raceways marcados na planta.
- O Intermediate Cross Connect situa-se no gruond floor na sala mais pratica para ligar aos outros edifícios devido a underground floor raceway, e a ligar ao horizontal Cross Connect do First Floor.
