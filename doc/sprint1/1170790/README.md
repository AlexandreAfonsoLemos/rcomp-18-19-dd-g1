RCOMP 2018-2019 Project - Sprint 1 - Member 1170790 folder
===========================================
(This folder is to be created/edited by the team member 1170790 only)

The owner of this folder (member number 1170790) will commit here all the outcomes (results/artifacts/products) of his/her work during sprint 1. This may encompass any kind of standard file types.

# EDIFICIO C (20x80)

## Proposta de esquema & Justificação
 
### Rés-do-chão

![C_GroundFloorPlanning.png](C_GroundFloorPlanning.png)

![C_GroundFloorPlanning_calhas.png](C_GroundFloorPlanning_calhas.png)


#### Descrição:
 
 * A sala C0.8 tem uma conexão com a vala técnica externa.

 * A altura do teto neste piso é 4 metros.

 * A sala C0.1 é uma despensa e, como não é aberto ao público, foi o lugar escolhido para ter o Telecommunication Enclosure deste piso.

 * Espaços comuns como os corredores não necessitam de tomadas pois a ligação à internet será feita através de routers Wi-Fi.

 * Foi colocado um Consolidation Point, na sala C0.5, para que o cabo não se estende-se mais do que 90 metros.

 * Foram também colocados dois Access Points cada um com 30 metros de raio para cobrir o piso todo e para minimizar o overlap.

* Foram usadas calhas que acolham 200 cabos cada, quer nas paredes quer nos caminhos já feitos no esquema.
 
### Primeiro Andar

![C_FirstFloorPlanning.png](C_FirstFloorPlanning.png)

![C_FirstFloorPlanning_calhas.png](C_FirstFloorPlanning_calhas.png)

#### Descrição:

* A sala C1.8 tem uma conexão com a vala técnica para o piso de baixo, e pode-se alocar aqui equipamento de infra-estrutura de rede.

 * A altura do teto neste piso é 3 metros, mas há um teto falso a 2.5 metros do chão que cobre o piso inteiro.

 * A sala C1.1 é uma despensa e, como não é aberto ao público, foi o lugar escolhido para ter o Telecommunication Enclosure deste piso.

 * Espaços comuns como os corredores não necessitam de tomadas pois a ligação à internet será feita através de routers Wi-Fi.

 * Foi colocado um Consolidation Point, na sala C1.5, para que o cabo não se estende-se mais do que 90 metros.

 * Foram também colocados dois Access Points cada um com 30 metros de raio para cobrir o piso todo e para minimizar o overlap.

 * Foi usado maioritariamente o teto falso para fazer a passagem dos cabos que em cada uma das salas descem pela parede até as tomadas, por calhas.

## Iventário

Notas : 

* Foi usado o standart de 2 outlets por 10 m^2, em que cada outlet tem 4 tomadas.

* Para os racks foi considerado que cada um obedece ao standart de 19'' (1U) e que no total temos 32U (Parket Tracer Rack).

* O inventário tem um acréscimo de 100% do hardware utilizado (exceto outlets) para a ligação de futuros equipamentos.

* Foi usado cabos de cobre UTP cat7 para as ligações dentro de cada edificio, pelo facto dos dispositivos de destino não suportarem fibra, e cabos de fibra óptica multimodo para a ligação entre-edifícios devido às distâncias, e as caracteristicas do cabo (maior rapidez na propagação da informação e menor ruído).

* Backbone: cabos usados para conectar os HC aos IC e este ao MC. HC (Horizontal Cross-Connect) estabele-se as ligações dos outlets ao telecomunication enclure, enquanto que o IC (Intermediate Cross-Connect) estabelece as ligações dos edificios ao MC (Main Cross-Connect).

* Redundância: é usada para impedir que as ligações entre Switches, no em caso de avaria ou mau funcionamento de um, vão todas a baixo. Para este fim, foram implementados sempre dois "caminhos" possíveis para chegar a um dispositivo final. 


### Rés-do-chão

#### OUTLETS

| SALA | ÁREA(m^2) | MIN TOMADAS (Prev) | OUTLETS UTILIZADOS |
| :------ | :--- | :--- | :--- |
| C0.1 | 22 | 6 | - |
| WC | 22 | 6 | - |
| C0.2 | 72 | 16 | 4 |
| C0.3 | 72 | 16 | 4 |
| C0.4 | 72 | 16 | 4 |
| C0.5 | 124 | 26 | 7 |
| C0.6 | 60 | 12 | 3 |
| C0.7 | 88 | 18 | 5 |
| C0.8 | 78 | 16 | 4 |
| C0.9 | 48 | 10 | 3 |
| C0.10 | 48 | 10 | 3 |
| C0.11 | 48 | 10 | 3 |
| TOTAL | | 150 | 40 |

#### CABO

| TOTAL CABO COBRE | TOTAL CABO FIBRA OPTICA | TOTAL CALHA
| :------ | :---- | :----
| 6155 m | 220 m | 520 m

*redundância incluída

#### OUTRO HARDWARE

| TELECOMUNICATION ENCLUSURES | 
| :------ 
| 2 |

* TELECOMUNICATION ENCLUSURE C0.1

| RACKS | PATCH PANELS DE COBRE DE 24 PORTAS | PATCH PANELS DE FIBRA DE 24 PORTAS | SWITCHS DE 48 PORTAS | SWITCHS DE 24 PORTAS |
| :------ | :---- | :---- | :---- | :------
| RACK 1 | 4 | 4 | 2 | 2* |

* TELECOMUNICATION ENCLUSURE C0.5

| RACKS | PATCH PANELS DE COBRE DE 24 PORTAS | PATCH PANELS DE FIBRA DE 24 PORTAS | SWITCHS DE 48 PORTAS | SWITCHS DE 24 PORTAS |
| :------ | :---- | :---- | :---- | :------
| RACK 1 | 10 | 10 | 5 | - |

*switches que se vão comportar como intermediate cross-connect.

### PRIMEIRO PISO

#### OUTLETS

| SALA | ÁREA(m^2) | MIN TOMADAS (Prev) | OUTLETS UTILIZADOS |
| :------ | :--- | :--- | :--- |
| C1.1 | 22 | 6 | - |
| WC | 22 | 6 | - |
| C1.2 | 72 | 16 | 4 |
| C1.3 | 72 | 16 | 4 |
| C1.4 | 72 | 16 | 4 |
| C1.5 | 124 | 26 | 7 |
| C1.6 | 60 | 12 | 3 |
| C1.7 | 88 | 18 | 5 |
| C1.8 | 44 | 10 | 3 |
| C1.9 | 65 | 14 | 4 |
| C1.10 | 61 | 14 | 4 |
| C1.11 | 48 | 10 | 3 |
| C1.12 | 48 | 10 | 3 |
| C1.13 | 48 | 10 | 3 |
| C1.14 | 48 | 10 | 3 |
| C1.15 | 52 | 12 | 3 |
| TOTAL |  | 192 | 53 |

#### CABO

| TOTAL CABO COBRE | TOTAL CABO FIBRA OPTICA | TOTAL CALHA
| :------ | :---- | :-----
| 7796 m (c/altura) | 227 m | 380 m

*redundância incluída

#### OUTRO HARDWARE

| TELECOMUNICATION ENCLUSURES | 
| :------ 
| 2 |

* TELECOMUNICATION ENCLUSURE C1.1

| RACKS | PATCH PANELS DE COBRE DE 24 PORTAS | PATCH PANELS DE FIBRA DE 24 PORTAS | SWITCHS DE 48 PORTAS |
| :------ | :---- | :---- | :----
| RACK 1 | 10 | 10 | 5 |

* TELECOMUNICATION ENCLUSURE C1.5

| RACKS | PATCH PANELS DE COBRE DE 24 PORTAS | PATCH PANELS DE FIBRA DE 24 PORTAS | SWITCHS DE 48 PORTAS |
| :------ | :---- | :---- | :----
| RACK 1 | 10 | 10 | 5 |

## Inventário total

 * 13 951 metros de cabo de cobre CAT6.
 * 447 metros de cabo de fibra optica.
 * 34 Patch Panels de cobre
 * 34 Patch Panels de fibra
 * 17 Switches de 48 portas
 * 2 Switches de 24 portas
 * 4 Racks
 * 4 Telecomunication Enclusures
 * 342 outlets