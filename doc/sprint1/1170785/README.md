RCOMP 2018-2019 Project - Sprint 1 - Member 1170785 folder
===========================================
(This folder is to be created/edited by the team member 1170785 only)

The owner of this folder (member number 1170785) will commit here all the outcomes (results/artifacts/products) of his/her work during sprint 1. This may encompass any kind of standard file types.
# EDIFICIO B (60x20)

## Proposta de esquema & Justificação
 
### Rés-do-chão

![B_GroundFloorPlanning](B_GroundFloorPlanning.png)

![B_GroundFloorPlanning_Calhas](B_GroundFloorPlanning_Calhas.png)

#### Descrição:
 
 * B0.1 é uma sala de arrumações onde não são requisitados fichas de rede, aplicando-se o mesmo as casas de banho.

 * A altura deste piso é 4 metros.

 * O rés do chão vem equipado com caminhos subterrâneos para cabos conectados à estrutura exterior.
 
 * Acesso aos caminhos subterrâneos estão marcados na planta do edifício.

 * A sala B0.1 por não ser aberto ao público e ser uma zona fora das áreas de trabalho, foi o lugar escolhido para ter o Telecommunication Enclosure deste piso.
 
 * Foi adicionado mais um Telecommunication Enclosure neste piso devido ao número de outlets excessivo e à área do andar.
 
 * Número de outlets por sala: 2 por área de trabalho + 2 por cada 10 metros quadrados.
 
 * Regras de distribuição de cabos: não exceder os 90m, passar junto as paredes, dar prioridade aos caminhos pré existentes(azul claro) e tentar usar caminhos comuns para minimizar o uso de calhas.
 
 * Regras de distribuição de outlets: assumindo cabos de rede com 5m por outlet, é necessário garantir que qualquer ponto da sala não se encontra a mais de 3 metros de um cabo. 
 
 * Foram usados 5 patch panels de 24 entradas para conectar 92 outlets.
 
 * Foram utilizados vários switch para cada patch panel para obter layer two switching, para evitar colisão de informação.
 
 * Foram utilizados cabos de cobre do tipo cat6 nos patch panels do horizontal crossconnect por uma questão de compatiblilidade com os dispositivos. Nos restantes paineis foi utilizado fibra para aumentar a transferência de dados e reduzir a interferência no sinal.
 
 * Regras de distribuição de access points: 2 access points cada um com um alcance de 30m em forma de donut foram colocados a 50m de distância para ter um overlap de 15% de forma a não perder ligação quando se muda de zona. Tem que se dar preferência as zonas perto do meio da sala para reduzir a ligação do wireless no exterior e os canais usados nos access points são 1,6 e 11 de forma alternada.
 
 
### Primeiro Andar

![B_FirstFloorPlanning.png](B_FirstFloorPlanning.png)

![B_FirstFloorPlanning_Calhas.png](B_FirstFloorPlanning_Calhas.png)

#### Descrição:

 * A altura deste piso é 3 metros, mas há um teto falso a 2.5 metros do chão que cobre o piso inteiro.

 * A sala B1.2 e B1.7 têm ambas um Telecommunication Enclosure devido à distribuição de outlets.
 
 * Número de outlets por sala: 2 por área de trabalho + 2 por cada 10 metros quadrados.
 
 * Regras de distribuição de cabos: não exceder os 90m, passar junto as paredes, dar prioridade aos caminhos pré existentes(azul claro) e tentar usar caminhos comuns para minimizar o uso de calhas.
 
 * Regras de distribuição de outlets: assumindo cabos de rede com 5m por outlet, é necessário garantir que qualquer ponto da sala não se encontra a mais de 3 metros de um cabo. 
 
 * Foram usados 7 patch panels de 24 entradas para conectar 120 outlets.
 
 * Foram utilizados vários switch para cada patch panel para obter layer two switching, para evitar colisão de informação.
 
 * Foram utilizados cabos de cobre do tipo cat6 nos patch panels do horizontal crossconnect por uma questão de compatiblilidade com os dispositivos. Nos restantes paineis foi utilizado fibra para aumentar a transferência de dados e reduzir a interferência no sinal.
 
 * Regras de distribuição de access points: 2 access points cada um com um alcance de 30m em forma de donut foram colocados a 50m de distância para ter um overlap de 15% de forma a não perder ligação quando se muda de zona. Tem que se dar preferência as zonas perto do meio da sala para reduzir a ligação do wireless no exterior e os canais usados nos access points são 1,6 e 11 de forma alternada.
 

## Iventário
  Notas:
 
  * Backbone: Conecção dos HC aos IC. O HC (Horizontal CrossConect) liga-se ao IC (Intermediate CrossConect) através de fibra. Para além disso, o HC estabele-se as ligações dos outlets ao telecomunication enclure através de cobre, o IC liga-se ao MC (Main CrossConect) e faz a ligação entre andares.

  * Redundância: Foram utilizados dois cabos por ligação para evitar perder a ligação no caso de falha de um dos cabos e, entre edifícios, foram utilizados vários caminhos para evitar a perda de ambos os cabos em caso de acidentes locais. 


### Rés-do-chão

#### OUTLETS

| SALA | ÁREA(m^2) | MIN OUTLETS | OUTLETS UTILIZADOS |
| :------ | :---- | :---------- | :----------- |
| b0.2 | 70.5 | 16 | 16 |
| b0.3 | 60.45 | 14 | 16 |
| b0.4 | 60.45 | 14 | 16 |
| b0.5 | 156.34 | 32 | 32 |
| TOTAL | | 76 | 80 |

#### CABO

| TOTAL CABO COBRE | TOTAL CABO FIBRA OPTICA | TOTAL CALHA |
| :------ | :---- | :----
| 1775.7 m | 204.9 m | 125.51 m |

#### OUTRO HARDWARE

| PATCH PANELS DE COBRE DE 24 PORTAS | TELECOMUNICATION ENCLUSURES | 
| :------ | :---- |
| 5 | 2 |

| RACK | PATCH PANELS DE COBRE DE 24 PORTAS | Patch Panel de fibra 16 portas  | Switchs de cobre 
| :------ | :---- | :------ | :------
|Rack 0.1 | 2 | 1 | 8 |
|Rack 0.5 | 3 | 0 | 9 |

### PRIMEIRO PISO

#### OUTLETS

| SALA | ÁREA(m^2) | MIN TOMADAS (Prev) | OUTLETS UTILIZADOS |
| :------ | :---- | :---------- | :----------- |
| b1.1 | 20.9 | 6 | 8 |
| b1.2 | 37.35 | 8 | 8 |
| b1.3 | 64.74 | 14 | 16 |
| b1.4 | 64.74 | 14 | 16 |
| b1.5 | 67.68 | 14 | 16 |
| b1.6 | 67.68 | 14 | 16 |
| b1.7 | 67.68 | 14 | 16 |
| b1.8 | 54.39 | 12 | 12 |
| b1.9 | 54.39 | 12 | 12 |
| TOTAL |  | 108 | 120 |

#### CABO

| TOTAL CABO COBRE | TOTAL CABO FIBRA OPTICA | Total Calha |
| :------ | :---- | :---- |
| 2439.6 m | 370.7 m | 173.9m

#### OUTRO HARDWARE

| PATCH PANELS DE COBRE DE 24 PORTAS | TELECOMUNICATION ENCLUSURES | 
| :------ | :---- |
| 7 | 2 |

| RACK | PATCH PANELS DE COBRE DE 24 PORTAS | Patch Panel de fibra 16 portas  | Switchs de cobre 
| :------ | :---- | :------ | :------
|Rack 1.2 | 3 | 0 | 9 |
|Rack 0.5 | 4 | 0 | 12 |

#### INVENTÁRIO TOTAL
 * 4215.2m de cabo de cobre
 * 299.41m de calha 
 * 12 patch panels de cobre 
 * 1 patch panel de fibra
 * 38 switches
 * 4 racks
 * 4 Telecomunication Enclusures
 * 212 outlets
