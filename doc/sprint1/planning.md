RCOMP 2018-2019 Project - Sprint 1 planning
===========================================
### Sprint master: 1170505 ###
(This file is to be created/edited by the sprint master only)
# 1. Sprint's backlog #
- T.1.1 Development of a structured cabling project for building A, and also encompassing the campus backbone. 

- T.1.2  Development of a structured cabling project for building B.

- T.1.3  Development of a structured cabling project for building C.

- T.1.4  Development of a structured cabling project for building D.

- T.1.5  Development of a structured cabling project for building E.

- T.1.6  Development of a structured cabling project for building F.

# 2. Technical decisions and coordination #
In this section, all technical decisions taken in the planning meeting should be mentioned. 		
Most importantly, all technical decisions impacting on the subtasks implementation must be settled on this meeting and specified here.

- Demonstration of calculations regarding the number of network outlets for each room. 

- Network outlets deployment schematic plan (including outlets for wireless access points) and justification comments. 

- Cross-connects deployment schematic plan and justification comments. 

- Cable pathways deployment schematic plan and justification comments. 

- Hardware inventories, including: total cable lengths by cable type, appropriate type patch panels, network outlets, telecommunication enclosures of suitable size.

# 3. Subtasks assignment #
(For each team member (sprint master included), the description of the assigned subtask in sprint 1)

- 1170505 - Structured cable design for building E.

- 1170508 - Structured cable design for building D.

- 1170785 - Structured cable design for building B.

- 1170790 - Structured cable design for building C.

- 1161071 - Structured cable design for building A.

- 1170803 - Structured cable design for building E.
