RCOMP 2018-2019 Project - Sprint 1 - Member 1161071 folder
===========================================
(This folder is to be created/edited by the team member 1161071 only)

The owner of this folder (member number 1161071) will commit here all the outcomes (results/artifacts/products) of his/her work during sprint 1. This may encompass any kind of standard file types.
# EDIFICIO A (40x20)

## Proposta de esquema & Justificação
 
### Rés-do-chão

![A_GroundFloorPlanning](A_GroundFloorPlanning.png)

#### Descrição: 
 
 * A0.1 é uma entrada onde não são requisitados fichas de rede exceto na mesa de entrada onde são necessários 5 outlets.
 
 * Não são requisitados outlets nas áreas publicas nem nas salas de arrumações.

 * A altura deste piso é 4 metros.

 * O rés do chão vem equipado com caminhos subterrâneos para cabos conectados à estrutura exterior.
 
 * Acesso aos caminhos subterrâneos estão marcados na planta do edifício.
 
 * Estão disponiveis multiplas passagens de cabo para o andar de cima.
 
 * Número de outlets por sala: 2 por área de trabalho + 2 por cada 10 metros quadrados.
 
 * Regras de distribuição de cabos: não exceder os 90m, passar junto as paredes, dar prioridade aos caminhos pré existentes(azul claro) e tentar usar caminhos comuns para minimizar o uso de calhas.
 
 * Regras de distribuição de outlets: assumindo cabos de rede com 5m por outlet, é necessário garantir que qualquer ponto da sala não se encontra a mais de 3 metros de um cabo.
 
 * Foram utilizados cabos de cobre do tipo cat6 nos patch panels do horizontal crossconnect por uma questão de compatiblilidade com os dispositivos. Nos restantes paineis foi utilizado fibra para aumentar a transferência de dados e reduzir a interferência no sinal.
 
 * Foi utilizado 1 access point com um alcance de 30m em forma de donut, porque é suficiente para cobrir o andar por completo e foi estimada uma densidade media de 20 pessoas no andar.
 
 
### Primeiro Andar

![A_FirstFloorPlanning.png](A_FirstFloorPlanning.png)

#### Descrição: 

 * Passagem de cabos para o piso de baixo através de pontos marcados na planta.
 
 * A altura deste piso é 3 metros.

 * A sala A1.4 é um datacenter e não são permitidos outlets.
 
 * A1.3 é uma sala de arrumações e não sao requisitados outlets.
 
 * Número de outlets por sala: 2 por área de trabalho + 2 por cada 10 metros quadrados.
 
 * Regras de distribuição de cabos: não exceder os 90m, passar junto as paredes, dar prioridade aos caminhos pré existentes(azul claro) e tentar usar caminhos comuns para minimizar o uso de calhas.
 
 * Regras de distribuição de outlets: assumindo cabos de rede com 5m por outlet, é necessário garantir que qualquer ponto da sala não se encontra a mais de 3 metros de um cabo. 
 
 * Foram usados 6 patch panels de 24 entradas de cobre para conectar 108 outlets.
 
 * Foram utilizados 3 switch para cada patch panel de cobre e 2 para cada patch panel de fibra para obter layer two switching, para evitar colisão de informação.
 
 * Foram utilizados cabos de cobre do tipo cat6 nos patch panels do horizontal crossconnect por uma questão de compatiblilidade com os dispositivos. Nos restantes paineis foi utilizado fibra para aumentar a transferência de dados e reduzir a interferência no sinal.
 
 * Foi utilizado 1 access point com um alcance de 30m em forma de donut, porque é suficiente para cobrir o andar por completo e foi estimada uma densidade media de 20 pessoas no andar.
 
 * 4 Telecomunication Enclosure no datacenter por estar fora do acesso ao público, por ter acesso ao andar de baixo, por o edifício ser pequeno e por ter espaço no datacenter.
 
 * Foi utilizada a topologia de estrela para evitar congestionamento na transferencia de informação e aumentar a realibilidade do sistema.
  
 * Por pedido o edificio house possui o main cross connect , que faz a ligação a todos os intermediate cross connects atraves de fibra. 
 
 * O Edificio A possui 1 intermediate cross connect que faz a ligação entre o main cross connect e os respetivos horizontal cross connect atraves de fibra.
 
 * O Edificio A possui tambem 2 horizontal cross connect em que cada faz a ligação dos respetivos outlets de cada andar ao intermediate cross connect atraves de cobre. 

 * Para redundancia foram utilizados dois cabos por ligação para evitar a perda de ligação no caso de falha de um dos cabos. Entre edificios foram utilizados caminhos diferentes para evitar a perda de ambos os cabos em caso de acidentes locais. 


## Iventário

### Rés-do-chão

#### OUTLETS

| SALA | ÁREA(m^2) | MIN OUTLETS | OUTLETS UTILIZADOS |
| :------ | :---- | :---------- | :----------- |
| a0.1 | 333.96 | 5 | 8 |
| a0.2 | 128.8 | 26 | 28 |
| a0.3 | 83.95 | 18 | 20 |
| TOTAL | | 49 | 56 |

#### CABO

| TOTAL CABO COBRE | TOTAL CABO FIBRA OPTICA |
| :------ | :---- |
| 1382 m | 3.5 m |

#### OUTRO HARDWARE

| RACK | PATCH PANELS DE COBRE DE 24 PORTAS | Patch Panel de fibra 16 portas  | | :------ | :---- |
| 0 | 0 |

### PRIMEIRO PISO

#### OUTLETS

| SALA | ÁREA(m^2) | MIN TOMADAS (Prev) | OUTLETS UTILIZADOS |
| :------ | :---- | :---------- | :----------- |
| b1.1 | 83.22 | 18 | 20 |
| b1.2 | 127.68 | 26 | 28 |
| b1.5 | 39.2 | 8 | 8 |
| TOTAL |  | 52 | 56 |

#### CABO

| TOTAL CABO COBRE | TOTAL CABO FIBRA OPTICA |
| :------ | :---- |
| 2268 m | 32 m |

#### OUTRO HARDWARE

| RACK | PATCH PANELS DE COBRE DE 24 PORTAS | Patch Panel de fibra 16 portas  | 
| :---- | :------ | :---- |
| rack0 | 0 | 1 |
| rack1 | 0 | 1 |
| rack2 | 3 | 0 |
| rack3 | 3 | 0 |