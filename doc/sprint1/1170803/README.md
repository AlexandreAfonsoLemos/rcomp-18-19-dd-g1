RCOMP 2018-2019 Project - Sprint 1 - Member 1170803 folder
===========================================
(This folder is to be created/edited by the team member 1170803 only)

The owner of this folder (member number 1170803) will commit here all the outcomes (results/artifacts/products) of his/her work during sprint 1. This may encompass any kind of standard file types.


# EDIFICIO F
### GROUND FLOOR

##### PROBLEMA

![BUILDING-F-F0.PNG](BUILDING-F-F0.PNG)

###### Descrição:
 A sala F0.8 tem uma conexão com a vala técnica externa. A altura do teto neste piso é de 5 metros, mas a área à esquerda dividida em salas tem um teto falso, colocado a 2,5 metros do chão, cobrindo toda aquela área. Não é obrigatório ter pontos de rede na área aberta do lado direito, nem em áreas comuns do lado esquerdo.

##### SOLUÇÃO

![ResoluçãoPiso0.PNG](ResoluçãoPiso0.PNG)

| DIVISÃO | ÁREAS | MIN OUTLETS | TAMANHO CABO |
| :------ | :---- | :---------- | :----------- |
| F0.1 | 47,97m^2 | 10 | 420m |
| F0.2 | 47,97m^2 | 10 | 576m |
| F0.3 | 47,97m^2 | 10 | 624m |
| F0.4 | 47,97m^2 | 10 | 744m |
| F0.5 | 47,97m^2 | 10 | 824m |
| F0.6 | 46,76m^2 | 10 | 332m |
| F0.7 | 46,76m^2 | 10 | 436m |
| F0.8 | 60,84m^2 | 14 | 148m |
| F0.9 | 47,97m^2 | 10 | 248m |
| F0.10 | 42,90m^2 | 10 | 324m |
| F0.11 | 42,90m^2 | 10 | 412m |
| WC LT | 27,72m^2 | 0 | - |
| WC RT | 27,72m^2 | 0 | - |

| EXTRAS | TAMANHO CABO |
| :----- | :----------- |
| ROUTERS | 102m |
| SUBIDAS P/ TETO | 10m |

TOTAL CABO COBRE: 5200m

TOTAL CALHA (CAPAC. 30 CABOS) : 310m

TOTAL ESTEIRA (CAPAC. 30 CABOS) : 22m

### FLOOR 1

##### PROBLEMA

![BUILDING-F-F1.PNG](BUILDING-F-F1.PNG)

###### Descrição:
A altura do teto neste piso é de 3 metros, mas há um teto falso colocado a 2,5 metros do chão, cobrindo todo este piso. As áreas comuns não são obrigadas a ter pontos de rede.

##### SOLUÇÃO

![ResoluçãoPiso1.PNG](ResoluçãoPiso1.PNG)

| DIVISÃO | ÁREAS | MIN OUTLETS | TAMANHO CABO |
| :------ | :---- | :---------- | :----------- |
| F1.1 | 48,65m^2 | 10 | 428m |
| F1.2 | 48,65m^2 | 10 | 596m |
| F1.3 | 48,65m^2 | 10 | 656m |
| F1.4 | 48,65m^2 | 10 | 756m |
| F1.5 | 48,65m^2 | 10 | 840m |
| F1.6 | 48,65m^2 | 10 | 888m |
| F1.7 | 48,65m^2 | 10 | 788m |
| F1.8 | 48,65m^2 | 10 | 732m |
| F1.9 | 48,65m^2 | 10 | 632m |
| F1.10 | 48,65m^2 | 10 | 480m |
| F1.11 | 48,30m^2 | 10 | 344m |
| F1.12 | 48,30m^2 | 10 | 444m |
| F1.13 | 48,30m^2 | 10 | 608m |
| F1.14 | 48,30m^2 | 10 | 464m |
| F1.15 | 48,30m^2 | 10 | 392m |
| F1.16 | 48,30m^2 | 10 | 248m |
| F1.17 | 62,57m^2 | 14 | 160m |
| F1.18 | 49,52m^2 | 10 | 240m |
| F1.19 | 62,57m^2 | 14 | 476m |
| F1.20 | 62,57m^2 | 14 | 588m |
| F1.21 | 62,57m^2 | 14 | 740m |
| F1.22 | 62,57m^2 | 14 | 480m |
| F1.23 | 62,57m^2 | 14 | 320m |
| F1.24 | 79,97m^2 | 16 | 216m |
| WC LT | 38,26m^2 | 0 | - |
| WC RT | 35,12m^2 | 0 | - |

| EXTRAS | TAMANHO CABO |
| :----- | :----------- |
| ROUTERS | 70m |
| SUBIDAS P/ TETO | 10m |

REDUNDÂNCIA ENTRE HCC PISO 1 E CP PISO 1(FIBRA): 140m

REDUNDÂNCIA ENTRE ICC PISO 0 E HCC PISO 1(FIBRA): 32m

TOTAL CABO COBRE: 12156m

TOTAL CABO FIBRA: 172m

TOTAL CALHA (CAPAC. 30 CABOS) : 744m

TOTAL ESTEIRA (CAPAC. 30 CABOS) : 8m

### INVENTÁRIO TOTAL

- 17356m cabo Cobre
- 172m cabo Fibra
- 1054m calha c/ capacidade 30 cabos
- 30m esteira c/ capacidade 30 cabos
- 4 Patch-Panels Fibra
- 20 Patch-Panels Cobre
- 4 Routers
- 104 Outlets

### INVENTÁRIO POR RACK

###### INTERMEDIATE CROSS-CONNECT PISO 0

- 1 Patch-Panel Fibra

###### HORIZONTAL CROSS-CONNECT PISO 0

- 1 Patch-Panel Fibra
- 6 Patch-Panels Cobre

###### HORIZONTAL CROSS-CONNECT PISO 1 (ESQUERDA)

- 1 Patch-Panel Fibra
- 7 Patch-Panels Cobre

###### CONSOLIDATION POINT PISO 1 (DIREITA)

- 1 Patch-Panel Fibra
- 7 Patch-Panels Cobre

###### Informação adicional:

- O Intermediate Cross-Connect foi colocado na sala F0.8 porque é por lá que chega o sinal.

- O Horizontal Cross-Connect do Piso 0 foi colocado na sala F0.8 para ficar no mesmo closet que  o Intermediate Cross-Connect. O Horizontal Cross-Connect do Piso 1 foi colocado na sala F1.17 porque é nessa sala que está localizada a passagem entre o Piso 0  e o Piso 1.

- O tamanho dos cabos entre o outlet e o Patch-Panel não pode ser superior a 90m por isso foi necessária a colocação de um Consolidation Point na sala F1.24, a cerca de 70m do Horizontal Cross-Connect. Assim os outlets do lado direito do Piso 1 conectam-se ao Consolidation Point e os outlets do lado esquerdo do Piso 1 conectam-se ao Horizontal Cross-Connect.

- Em cada sala era necessário ter no mínimo 2 outlets e depois, por cada 10m^2 adicionava-se mais 2 outlets. Ex: 5m^2 -> 2 outlets 15 m^2 - 4 outlets 55m^2 -> 12 outlets
