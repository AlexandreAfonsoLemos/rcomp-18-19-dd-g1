RCOMP 2018-2019 Project - Sprint 1 - 1170508
===========================================

# ** Edifício D **

Este edifício tem 2 andares.

A área total de cada andar é 2400 metros quadrados (80 metros de largura e 30 metros de comprimento).

# Medições e número padrão de outlets em cada área

## - Primeiro andar

![Floor0](Floor0.png)

A área de trabalho deste andar é 990 metros quadrados (33 metros de largura e 30 metros de comprimento), visto que uma larga área comum não necessita de outlets. Assim sendo, vai ter um itermediate cross-connect que conecta com o main que esta no edificio A e um horizontal cross-connect (visto que é aconselhável o uso de um a cada 1000 metros quadrados).

| Compartimentos | Comprimento de cabo para outlets (m) | Número de outlets utilizados
| --------------- | ------ | ------ | ---- |
| D0.1 | 66,36 | 12 |
| D0.2 | 208 | 16 |
| D0.3 | 306 | 16 |  
| D0.4 | 442,6 | 16 |
| D0.5 | 545,48 | 16 |
| D0.6 | 306,28 | 16 |
| D0.7 | 438,52 | 16 |
| D0.8 | 543,76 | 16 |
| D0.9 | 771,6| 16 |
| D0.10 | 791,64 | 16 |
| D0.11 | 952,6 | 16 ||

Os access points (AP) vão ficar colocados nos compartimentos D0.3 e D0.9, conseguindo assim um sinal forte em toda a área de trabalho, visto que cada um tem um alcance de 25 metros em seu redor.

## - Segundo andar

![Floor1](Floor1.png)

No segundo andar, a área de trabalho é equivalente à área total do andar.

Assim, sendo a área total 2400 metros quadrados é necessário o uso de 3 horizontal cross-connects, de modo a respeitar as regras impostas.

| Compartimentos | Comprimento cabo para outlets (m) | Número de outlets utilizados
| --------------- | ------ | ---- |
| D1.1 |  75,92  | 12 |
| D1.2 | 173,04   | 16 |
| D1.3 |  66,06  | 16 |
| D1.4 |  411  | 16 |
| D1.5 |  514,88  | 16 |
| D1.6 |  390,28  | 16 |
| D1.7 |  286,88  | 16 |
| D1.8 |  61  | 12 |
| D1.9 |  197,64 | 16 |
| D1.10 |  322,64 | 16 |
| D1.11 | 309,84  | 16 |
| D1.12 | 442,8  | 16 |
| D1.13 | 249  | 16 |
| D1.14 | 253,96 | 16 |
| D1.15 | 410  | 16 |
| D1.16 |  273,96  | 16 |
| D1.17 | 544 | 16 |
| D1.18 |  614,68 | 16 |
| D1.19 | 490,08  | 16 |
| D1.20 | 343.8 | 16 |
| D1.21 | 109,12 | 16 |
| D1.22 | 221,56 | 16 |
| D1.23 | 409,36 | 16 |
| D1.24 | 343,04 | 16 ||

Os AP do segundo andar vão ficar colocados nos compartimentos D1.11, D1.13 e D1.16. Decidi colocar 3 access points, porque dado a área de trabalho ser bastante superior à do primeiro piso é mais seguro colocar mais um do que no piso inferior, para o acesso à rede ser uniforme por todo o andar. Além disso, como todos os AP deste piso irão ser colocados na parte central, o sinal vai ser forte em todo o piso, não havendo compartimentos onde o sinal é menos forte.

### Justificação do número de outlets e sua colocação em cada compartimento

Em todos os compartimentos do primeiro andar, exceto o D0.8, o número padrão de outlets é 10 (visto que as suas áreas se situam entre os 40 e 50 metros quadrados). Apesar disso vão ser colocados 16 outlets em cada um deles, para o sinal ser uniforme em todas as divisões. Visto que estão agrupados em 4 outlets serão distribuídos de forma a que em todos os pontos da sala esteja disponível um outlet a menos de 3 metros. Com esta colocação dos outlets vai haver em todos os compartimentos uma boa divisão de sinal por todo o espaço disponível.

Para o segundo vamos usar a mesma forma de pensar que no primeiro andar.
Este piso tem compartimentos maiores que o primeiro e consequentemente o numero padrao de outlets destes vai ser maior. Assim, em alguns o numero de outlets padrao vai ser igual aos colocados.

### Localização dos cross-connects

No primeiro andar, visto que a área de trabalho é inferior a 1000 metros quadrados é apenas necessário um horizontal cross-connect. Este ficará no compartimento D0.1.

No segundo andar, vão ser necessários 3 horizontal cross-connects, tendo em conta que a área de trabalho é bastante superior. Estes ficarão nos compartimentos 1, 8 e 21 para uma melhor cobertura.


### Caminhos dos cabos

Os cabos vão passar através do espaço entre o teto falso e o teto. Vão estar agrupados e dentro de calhas. A entrada de cada sala vai haver uma calha que vem do teto ate ao chão e dai ate aos outlets também vai estar em calhas.

### Tipos de cabos
Vão ser utilizados dois tipos de cabos:
- ##### Fibra
Para as ligações entre cross-connects
- ###### CAT7
Para as ligações com os outlets e os dispositivos pretendidos.

#  Inventário  

## Primeiro piso
- 2 AP

- 176 outlets

- 1 Telecomunication enclosure

- 5420 metros de cobre CAT7

- 280 metros de calha

- 2 racks

- 8 patch panels de cobre
- 2 patch panel de fibra

- ### Rack1 (ICC)
  - 1 patch panel de fibra

- ### Rack2 (HCC)
  - 8 patch panels de cobre
  - 1 patch panel de fibra



## Segundo piso

- 8517 metros de cobre CAT7

- 700 metros de calha

- 3 AP

- 258,86 metros de Fibra (usados para as ligacoes entre os IC e HC, supomos que o main esta no edificio A e que tem um ligacao com o edificio D. Vamos utilizar 2 cabos de fibra para cada ligação ICC e HCC para que haja redundância)

- 382 outlets

- 3 Telecomunication enclosures

- 3 racks

- 18 patch panels de cobre

- 3 patch panels de fibra

- ### Rack4 (HCC)
  - 6 patch panels de cobre
  - 1 patch panel de fibra

- ### Rack5 (HCC)
  - 6 patch panels de cobre
  - 1 patch panel de fibra

- ### Rack6 (HCC)
    - 6 patch panels de cobre
    - 1 patch panel de fibra
