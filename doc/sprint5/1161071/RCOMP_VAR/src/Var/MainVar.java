/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Var;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import static Var.UdpClient.printIPs;
import static Var.UdpClient.sendToAll;

/**
 *
 * @author Daniel
 */
public class MainVar {

    private static final String AUTH_ADDR = "172.18.152.150";
    private static final String HASH_ADDR = "172.18.158.86";
    private static final String STACK_ADDR = "172.18.155.213";
    private static final String TCP_ADDR = "172.18.156.152";
    private static final String CONVERT_ADDR = "172.18.152.100";

    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static InetAddress address;
    static DatagramSocket sock;
    private static final int SERVICE_PORT = 30401;
    private static final int SLEEP = 80;
    private static final String VAR_HOST_NAME = "2;VAR";
    private static final String DEL = ";";

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) throws Exception {
        UdpClient.addAllIPs();
        String nick, frase;
        byte[] data = new byte[300];
        byte[] fraseData;
        int i;
        DatagramPacket udpPacket;
        try {
            sock = new DatagramSocket(SERVICE_PORT);
        } catch (IOException ex) {
            System.out.println("Failed to open local port");
            System.exit(1);
        }

        VarRepository rep = new VarRepository();

        address = InetAddress.getByName(HASH_ADDR);
        udpPacket = new DatagramPacket("1".getBytes(), 1, address, SERVICE_PORT);
        sock.send(udpPacket);

        address = InetAddress.getByName(AUTH_ADDR);
        udpPacket = new DatagramPacket("1".getBytes(), 1, address, SERVICE_PORT);
        sock.send(udpPacket);

        address = InetAddress.getByName(STACK_ADDR);
        udpPacket = new DatagramPacket("1".getBytes(), 1, address, SERVICE_PORT);
        sock.send(udpPacket);

        address = InetAddress.getByName(CONVERT_ADDR);
        udpPacket = new DatagramPacket("1".getBytes(), 1, address, SERVICE_PORT);
        sock.send(udpPacket);

        address = InetAddress.getByName(TCP_ADDR);
        udpPacket = new DatagramPacket("1".getBytes(), 1, address, SERVICE_PORT);
        sock.send(udpPacket);

        //-------------------------------------------------//--------------------------------------------------
//        varAddress = InetAddress.getByName(VAR_ADDR);
//        udpPacket = new DatagramPacket(VAR_HOST_NAME.getBytes(), VAR_HOST_NAME.length(), varAddress, SERVICE_PORT);
//        sock.send(udpPacket);
        //------------------------------------------------//---------------------------------------------------
        while (true) { // handle user inputs
            Thread udpReceiver = new Thread(new UdpServer(sock));
            udpReceiver.start();
            frase = Menu.showFirst();

            if (frase.compareTo("1") == 0) {
                //Var Menu
                String option = "";
                while (!option.equals("EXIT")) {
                    Thread.sleep(SLEEP);
                    String name;
                    String content;
                    option = Menu.showVARMenu();
                    switch (option) {
                        case ("1"): //store index 4
                            System.out.println("Name:\n ");
                            name = in.readLine();
                            System.out.println("Content:\n");
                            content = in.readLine();
                            byte[] store = rep.store(name.getBytes(), content.getBytes());
                            System.out.println(new String(store, 0, store.length));
                            break;
                        case ("2"): //fetch index 5
                            System.out.println("Name:\n ");
                            name = in.readLine();
                            byte[] fetch = rep.fetch(name.getBytes());
                            System.out.println(new String(fetch, 0, fetch.length));
                            break;
                        case ("3"): //list index 6
                            System.out.println("Size:\n");
                            int size = Integer.parseInt(in.readLine());
                            String names = rep.list(size);
                            String list[] = names.split(DEL);
                            for (i = 0; i < list.length; i++) {
                                System.out.println(list[i] + "\n");
                            }
                            break;
                        case ("4"): //erase index 7
                            System.out.println("Name:\n");
                            name = in.readLine();
                            byte[] erase = rep.erase(name.getBytes());
                            System.out.println(new String(erase, 0, erase.length));
                            break;
                        default:
                            System.out.println("\nInvalid Option!");
                    }
                }
            }

            if (frase.compareTo("2") == 0) {
                if (UdpClient.checkIfExists(AUTH_ADDR)) {
                    //AutchUserMD5 Menu
                    String option = "";
                    while (!option.equals("EXIT")) {
                        Thread.sleep(SLEEP);
                        option = Menu.showAUTHMenu();
                        switch (option) {
                            case ("1"): //log In index 4
                                System.out.println("UserName: ");
                                String username = in.readLine();
                                System.out.println("Password: ");
                                String password = in.readLine();

                                frase = "4" + DEL + username + DEL + password;
                                fraseData = frase.getBytes();
                                udpPacket.setData(fraseData);
                                udpPacket.setLength(frase.length());
                                UdpClient.send(sock, udpPacket, "AUTH");
                                Thread.sleep(SLEEP);
                                break;
                            default:
                                System.out.println("\nInvalid Option!");
                        }
                    }
                } else {
                    System.out.println("No available connection!");
                }
            }

            if (frase.compareTo("3") == 0) {
                if (UdpClient.checkIfExists(HASH_ADDR)) {
                    //Hash Menu
                    String option = "";
                    while (!option.equals("EXIT")) {
                        Thread.sleep(SLEEP);
                        String toHash;
                        String type;
                        option = Menu.showHASHMenu();
                        switch (option) {
                            case ("1"): //digest index 4
                                System.out.println("String:\n");
                                toHash = in.readLine();
                                System.out.println("Type:\n");
                                type = in.readLine();

                                frase = "4" + DEL + type + DEL + toHash;
                                fraseData = frase.getBytes();
                                udpPacket.setData(fraseData);
                                udpPacket.setLength(frase.length());
                                UdpClient.send(sock, udpPacket, "HASH");
                                break;
                            case ("2"): //list index 5
                                frase = "5";
                                fraseData = frase.getBytes();
                                udpPacket.setData(fraseData);
                                udpPacket.setLength(frase.length());
                                UdpClient.send(sock, udpPacket, "HASH");
                                break;
                            default:
                                System.out.println("\nInvalid Option!");
                        }
                    }
                } else {
                    System.out.println("No available connection!");
                }
            }

            if (frase.compareTo("4") == 0) {
                if (UdpClient.checkIfExists(CONVERT_ADDR)) {
                    //NumConvert Menu
                    String option = "";
                    while (!option.equals("EXIT")) {
                        Thread.sleep(SLEEP);
                        String IR;
                        String IOR;
                        String conv;
                        option = Menu.showCOVERTMenu();
                        switch (option) {
                            case ("1"): //convert index 4
                                System.out.println("IR:\n");
                                IR = in.readLine();
                                System.out.println("IOR:\n");
                                IOR = in.readLine();
                                System.out.println("String to convert:\n");
                                conv = in.readLine();

                                frase = "4" + DEL + IR + DEL + conv + DEL + IOR;
                                fraseData = frase.getBytes();
                                udpPacket.setData(fraseData);
                                udpPacket.setLength(frase.length());
                                UdpClient.send(sock, udpPacket, "CONVERT");
                                break;
                            case ("2"): //list index 5
                                frase = "5";
                                fraseData = frase.getBytes();
                                udpPacket.setData(fraseData);
                                udpPacket.setLength(frase.length());
                                UdpClient.send(sock, udpPacket, "CONVERT");
                                break;
                            default:
                                System.out.println("\nInvalid Option!");
                        }
                    }
                } else {
                    System.out.println("No available connection!");
                }
            }

            if (frase.compareTo("5") == 0) {
                if (UdpClient.checkIfExists(STACK_ADDR)) {
                    //Stack Menu
                    String option = "";
                    while (!option.equals("EXIT")) {
                        Thread.sleep(SLEEP);
                        String id;
                        String content;
                        option = Menu.showSTACKMenu();
                        switch (option) {
                            case ("1"): //push index 4
                                System.out.println("Name:\n ");
                                id = in.readLine();
                                System.out.println("Content:\n");
                                content = in.readLine();

                                frase = "4" + DEL + id + DEL + content;
                                fraseData = frase.getBytes();
                                udpPacket.setData(fraseData);
                                udpPacket.setLength(frase.length());
                                UdpClient.send(sock, udpPacket, "STACK");
                                break;
                            case ("2"): //pop index 5
                                System.out.println("Name:\n ");
                                id = in.readLine();

                                frase = "5" + DEL + id;
                                fraseData = frase.getBytes();
                                udpPacket.setData(fraseData);
                                udpPacket.setLength(frase.length());
                                UdpClient.send(sock, udpPacket, "STACK");
                                break;
                            case ("3"): //list index 6
                                frase = "6";
                                fraseData = frase.getBytes();
                                udpPacket.setData(fraseData);
                                udpPacket.setLength(frase.length());
                                UdpClient.send(sock, udpPacket, "STACK");
                                break;
                            default:
                                System.out.println("\nInvalid Option!");
                        }
                    }
                } else {
                    System.out.println("No available connection!");
                }
            }

            if (frase.compareTo("6") == 0) {
                if (UdpClient.checkIfExists(TCP_ADDR)) {
                    //TCPmonitor Menu
                    String option = "";
                    while (!option.equals("EXIT")) {
                        Thread.sleep(SLEEP);
                        String id;
                        String ip;
                        String portNR;
                        option = Menu.showTCPMenu();
                        switch (option) {
                            case ("1"): //addService index 4
                                System.out.println("Identifier:\n");
                                id = in.readLine();
                                System.out.println("IP address:\n");
                                ip = in.readLine();
                                System.out.println("Port Number:\n");
                                portNR = in.readLine();

                                frase = "4" + DEL + id + DEL + ip + DEL + portNR;
                                fraseData = frase.getBytes();
                                udpPacket.setData(fraseData);
                                udpPacket.setLength(frase.length());
                                UdpClient.send(sock, udpPacket, "TCP");
                                break;
                            case ("2"): //removeService index 5
                                System.out.println("Identifier:\n");
                                id = in.readLine();

                                frase = "5" + DEL + id;
                                fraseData = frase.getBytes();
                                udpPacket.setData(fraseData);
                                udpPacket.setLength(frase.length());
                                UdpClient.send(sock, udpPacket, "TCP");
                                break;
                            case ("3"): //serviceStatus index 6
                                frase = "6";
                                fraseData = frase.getBytes();
                                udpPacket.setData(fraseData);
                                udpPacket.setLength(frase.length());
                                UdpClient.send(sock, udpPacket, "TCP");
                                break;
                            default:
                                System.out.println("\nInvalid Option!");
                        }
                    }
                } else {
                    System.out.println("No available connection!");
                }
            }

            if (frase.compareTo("EXIT") == 0) {
                break;
            }
            if (frase.compareTo("LIST") == 0) {
                System.out.print("Active peers:");
                printIPs();
                System.out.println("");
            } else {
                System.out.println("\nInvalid Option!");
            }
        }
        address = InetAddress.getByName(HASH_ADDR);
        udpPacket = new DatagramPacket("0".getBytes(), 1, address, SERVICE_PORT);
        sock.send(udpPacket);

        address = InetAddress.getByName(AUTH_ADDR);
        udpPacket = new DatagramPacket("0".getBytes(), 1, address, SERVICE_PORT);
        sock.send(udpPacket);

        address = InetAddress.getByName(STACK_ADDR);
        udpPacket = new DatagramPacket("0".getBytes(), 1, address, SERVICE_PORT);
        sock.send(udpPacket);

        address = InetAddress.getByName(CONVERT_ADDR);
        udpPacket = new DatagramPacket("0".getBytes(), 1, address, SERVICE_PORT);
        sock.send(udpPacket);

        address = InetAddress.getByName(TCP_ADDR);
        udpPacket = new DatagramPacket("0".getBytes(), 1, address, SERVICE_PORT);
        sock.send(udpPacket);

        sock.close();
        //udpReceiver.join(); // wait for the UdpChatReceive thread to end
    }

    private static class Menu {

        private static final String HEADLINE_FIRST_MENU = ":---------- MENU ----------:";
        private static final String HEADLINE_VAR_MENU = ":---------- VAR ----------:";
        private static final String HEADLINE_AUTH_MENU = ":---------- AUTH USER MD5 ----------:";
        private static final String HEADLINE_HASH_MENU = ":---------- HASH ----------:";
        private static final String HEADLINE_CONVERT_MENU = ":---------- NUM CONVERT ----------:";
        private static final String HEADLINE_STACK_MENU = ":---------- STACK ----------:";
        private static final String HEADLINE_TCP_MENU = ":---------- TCP MONITOR ----------:";

        public Menu() {
        }

        private static String showFirst() throws IOException {
            System.out.println(HEADLINE_FIRST_MENU);
            System.out.println("\n1 - Var"
                    + "\n2 - AuthUserMD5"
                    + "\n3 - Hash"
                    + "\n4 - NumConvert"
                    + "\n5 - Stack"
                    + "\n6 - TCPmonitor"
                    + "\nEXIT - leave application"
                    + "\nLIST - list active peers"
                    + "\n"
                    + "\nChoose:\n");
            return in.readLine();
        }

        private static String showVARMenu() throws IOException {
            System.out.println(HEADLINE_VAR_MENU);
            System.out.println("\n1 - Store"
                    + "\n2 - Fetch"
                    + "\n3 - List"
                    + "\n4 - Erase"
                    + "\nEXIT - Back"
                    + "\n"
                    + "\nChoose:\n");
            return in.readLine();
        }

        private static String showAUTHMenu() throws IOException {
            System.out.println(HEADLINE_AUTH_MENU);
            System.out.println("\n1 - LogIn"
                    + "\nEXIT - Back"
                    + "\n"
                    + "\nChoose:\n");
            return in.readLine();
        }

        private static String showHASHMenu() throws IOException {
            System.out.println(HEADLINE_HASH_MENU);
            System.out.println("\n1 - Digest"
                    + "\n2 - List"
                    + "\nEXIT - Back"
                    + "\n"
                    + "\nChoose:\n");
            return in.readLine();
        }

        private static String showCOVERTMenu() throws IOException {
            System.out.println(HEADLINE_CONVERT_MENU);
            System.out.println("\n1 - Convert"
                    + "\n2 - List"
                    + "\nEXIT - Back"
                    + "\n"
                    + "\nChoose:\n");
            return in.readLine();
        }

        private static String showSTACKMenu() throws IOException {
            System.out.println(HEADLINE_STACK_MENU);
            System.out.println("\n1 - Push"
                    + "\n2 - Pop"
                    + "\n3 - List"
                    + "\nEXIT - Back"
                    + "\n"
                    + "\nChoose:\n");
            return in.readLine();
        }

        private static String showTCPMenu() throws IOException {
            System.out.println(HEADLINE_TCP_MENU);
            System.out.println("\n1 - Add Service"
                    + "\n2 - Remove Service"
                    + "\n3 - Service Status"
                    + "\nEXIT - Back"
                    + "\n"
                    + "\nChoose:\n");
            return in.readLine();
        }

    }
}
