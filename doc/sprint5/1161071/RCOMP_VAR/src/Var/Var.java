/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Var;

import java.util.Arrays;

/**
 *
 * @author 1161071
 */
public class Var {

    private static final int ID_RANGE = 80;
    private static final int CONTENT_RANGE = 400;

    private byte[] name;
    private byte[] content;

    /**
     * Var Construtior
     *
     * @param name identifier
     * @param content message content
     */
    public Var(byte[] name, byte[] content) {
        this.name = name;
        this.content = content;
    }

    /**
     * Return identifier
     *
     * @return identifier
     */
    public byte[] getName() {
        return name;
    }

    /**
     * Returns message content
     *
     * @return message content
     */
    public byte[] getContent() {
        return content;
    }

    /**
     * Adds a content to the Var
     *
     * @param content message content
     */
    public void setContent(byte[] content) {
        this.content = content;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Var other = (Var) obj;
        return Arrays.equals(this.name, other.name);
    }
}
