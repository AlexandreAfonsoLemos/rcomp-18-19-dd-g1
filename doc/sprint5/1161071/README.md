RCOMP 2018-2019 Project - Sprint 5 - Member 1161071 folder
===========================================
(This folder is to be created/edited by the team member 1161071 only)

The owner of this folder (member number 1161071) will commit here all the outcomes (results/artifacts/products) of his/her work during sprint 5. This may encompass any kind of standard file types.

#Explicação Adicional - VAR

* This application is intended for non-persistent storing of plain text contents (possibly with multiple lines), non-persistent means when the application stops running, data is lost. Each contents size is limited to 400 bytes (CRs included).

* Each text content to be stored is identified by a name (up to 80 characters long, case sensitive), the input commands to be supported are:

	**1.** Store - Given an identifier and a text content, stores it. As response, the same text content should be returned. If the identifier already exists, then its content is overwritten. If the content is empty, then the identifier is removed.
	
	**2.** Fetch - Given an identifier, fetches its content. As response, the stored text content should be returned. If the identifier doesn’t exist an error message should be returned.
	
	**3.** List - As response, a list of identifiers should be provided. The list should be truncated if it exceeds the maximum message size, in that case there should be a remark about that in the response message.
	
	**4.** Erase - Given an identifier, fetches its content and removes the identifier/content. As response, the stored text content should be returned. If the identifier doesn’t exist an error message should be returned