RCOMP 2018-2019 Project - Sprint 5 - Member 1170508 folder
===========================================
(This folder is to be created/edited by the team member 1170508 only)

The owner of this folder (member number 1170508) will commit here all the outcomes (results/artifacts/products) of his/her work during sprint 5. This may encompass any kind of standard file types.
