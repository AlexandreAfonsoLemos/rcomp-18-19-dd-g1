RCOMP 2018-2019 Project - Sprint 5 protocol
===========================================
### Sprint master: 1170785 ###
(This file is to be created/edited by the sprint master only)
# MENU STRUCTURE #

# MESSAGES SEND IN PAKETS #

# SEND #

	* [NUM];[PARAM_1];[PARAM_2];...;[PARAM_N]
	
		* [NUM] -> Index of the operation in the system reference at the command.
		* [PARAM_1_2_N] -> Parameters used by a particulary command.
		* NOTE: We used ";" to separate individually parts.
	
	* Command for add the address for a new active application: 1*

	* Command for delete address for a deactivated application: 0*
	
*The address is not on the message but can be obtain by a method in java.
		
# RECEVIED #

	* [REPLY]
		
			* [REPLY] -> Response for each command. The same for each application.
			


## 1. VAR ##

* Command for store a identifier and content: 3;[ID];[CONTENT]
	
* Command for fetch the content of a identifier: 4;[ID]
	
* Command for list all identifiers with particulary size: 5;[SIZE]
	
* Command for erase the identifier and content: 6;[ID]

## 2. UserAuthMD5 ##
	
* Command for log in an user: 3;[USER];[PASSWORD]
	
## 3. Hash ##

* Command for digeste a string: 3;[STRING_TO_HASH];[ALGORITHM_TYPE]
	
* Command for list the options to digest: 3

## 4. NumConvert ##

* Command for convert a number: 3;[FORMAT_INPUT];[NUM];[FORMAT_OUTPUT]
	
* Command for list the options to convert: 3

## 5. Stack ##

* Command for push: 3;[STACK_ID];[CONTENT]

* Command for pop: 3;[STACK_ID]
	
* Command for list id of stack: 3

## 6. TCPMonitor ##

* Command for add a service: 3;[ID];[IP_ADDRESS];[PORT_NUMBER]

* Command for remove a service: 3;[ID]

* Command for list the services: 3

## 7. EXIT ##
	* Exit application

## 8. LIST ##
	* List active peers

