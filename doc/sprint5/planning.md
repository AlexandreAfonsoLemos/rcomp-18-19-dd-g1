RCOMP 2018-2019 Project - Sprint 5 planning
===========================================
### Sprint master: 1170785 ###
(This file is to be created/edited by the sprint master only)
# 1. Sprint's backlog #
  * Take part on the design of the core protocol specification and develop a shared implementation for it (co-development). Establish the usermessages format for the Var application (3.1.), and implement such an application.
  
  * Take part on the design of the core protocol specification and develop a shared implementation for it (co-development). Establish the usermessages format for the UserAuthMD5 application (3.2.), and implement such an application.

  * Take part on the design of the core protocol specification and develop a shared implementation for it (co-development). Establish the usermessages format for the Hash application (3.3.), and implement such an application.

  * Take part on the design of the core protocol specification and develop a shared implementation for it (co-development). Establish the usermessages format for the NumConvert application (3.4.), and implement such an application.

  * Take part on the design of the core protocol specification and develop a shared implementation for it (co-development). Establish the usermessages format for the Stack application (3.5), and implement such an application.

  * Take part on the design of the core protocol specification and develop a shared implementation for it (co-development). Establish the usermessages format for the TCPmonitor application (3.6), and implement such an application.


# 2. Technical decisions and coordination #
  * UDP port number - 30401
  
  * Language used - Java
  
# 3. Subtasks assignment #
  * 1161071 - Var application
  
  * 1170785 - UserAuthMD5 application
  
  * 1170790 - Hash application + Connection between UDP client and UDP server + In charge of doing the code commits into an application folder.
  
  * 1170508 - NumConvert 
  
  * 1170505 -  Stack application
  
  * 1170803 - TCPmonitor application
