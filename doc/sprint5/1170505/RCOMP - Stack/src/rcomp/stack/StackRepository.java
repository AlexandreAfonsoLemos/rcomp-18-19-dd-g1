/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rcomp.stack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Daniel
 */
public class StackRepository {

    private List<Stack> stacks;
    private int size;

    public StackRepository(int size) {
        this.size = size;
        this.stacks = new ArrayList<>();
    }

    public int push(byte id[], byte content[]) {
        if (isStackFull()) {
            System.out.println("\nStack is Full");
            return 0;
        } else {
            Stack stack = new Stack(id);
            stack.addContentToStack(content);
            if (!stacks.contains(stack)) {
                this.stacks.add(stack);
                return stacks.size();
            } else {
                for (int i = 0; i < stacks.size(); i++) {
                    if (Arrays.equals(stacks.get(i).getStackIdentifier(), id)) {
                        stack.addContentToStack(content);
                        stacks.remove(stacks.get(i));
                        stacks.add(stack);
                        return stacks.size();
                    }
                }
            }
        }
        return -1;
    }

    public byte[] pop(byte[] id) {
        if (isStackEmpty()) {
            System.out.println("\nStack is Empty ");
            return "Empty".getBytes();
        } else {
            Stack stack = new Stack(id);
            if (!stacks.contains(stack)) {
                return "Stack identifier not found!".getBytes();
            } else {
                for (int i = 0; i < stacks.size(); i++) {
                    stack = stacks.get(i);
                    if (Arrays.equals(stack.getStackIdentifier(), id)) {
                        byte[] removeContent = stack.removeContent();
                        if (stack.isEmpty()) {
                            stacks.remove(stack);
                        }
                        return removeContent;
                    }
                }
            }
        }
        return null;
    }

    public byte[] list() {
        String response = "";
        for (int i = 0; i < stacks.size(); i++) {
            response = response + new String(stacks.get(i).getStackIdentifier(), 0, stacks.get(i).getStackIdentifier().length) + "-" + stacks.get(i).getPile().size() + ",";
        }
        return response.getBytes();
    }

    public boolean isStackEmpty() {
        return (this.stacks.isEmpty());
    }

    public boolean isStackFull() {
        return stacks.size() == size;
    }

}
