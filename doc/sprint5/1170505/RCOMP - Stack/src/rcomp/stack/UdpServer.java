/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rcomp.stack;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.security.NoSuchAlgorithmException;
import java.util.List;

/**
 *
 * @author Daniel
 */
public class UdpServer implements Runnable {

    private DatagramSocket s;
    private static final int SERVICE_PORT = 30401;
    private static final String DEL = ";";
    private static final String ALGORITH_ERROR = "[ERROR]: No Available Algorithm";

    public UdpServer(DatagramSocket udp_s) {
        s = udp_s;
    }

    public void run() {
        int i;
        byte[] data_rec = new byte[300], data_send = new byte[300];
        String frase, id;
        DatagramPacket p_rec;
        DatagramPacket p_send;
        StackRepository rep = new StackRepository(100);
        InetAddress currPeerAddress;
        p_rec = new DatagramPacket(data_rec, data_rec.length);
        p_send = new DatagramPacket(data_send, data_send.length);
        while (true) {
            //p_rec.setLength(data_rec.length);
            try {
                s.receive(p_rec);
            } catch (IOException ex) {
                return;
            }
            currPeerAddress = p_rec.getAddress();
            frase = new String(p_rec.getData(), 0, p_rec.getLength());
            String command[] = frase.split(DEL);
            switch (command[0]) {
                case "1":
                    // peer start
                    UdpClient.addIP(p_rec.getAddress());
                    try {
                        s.send(p_rec);
                    } catch (IOException ex) {
                        return;
                    }
                    break;
                case "0":
                    // peer exit
                    UdpClient.remIP(p_rec.getAddress());
                    break;
                case "2":
                    //ADD ADDRESS WITH HOST
                    UdpClient.addIP(command[1], p_rec.getAddress().getHostAddress());
                    break;
                case "3":
                    //RESPONCES TO VAR
                    break;
                case "4":
                    //PUSH

                    id = command[1];
                    String content = command[2];

                    int size = 0;
                    int push = rep.push(id.getBytes(), content.getBytes());

                    p_rec = new DatagramPacket(("" + push).getBytes(), ("" + push).getBytes().length, currPeerAddress, SERVICE_PORT);
                    try {
                        s.send(p_rec);
                    } catch (IOException ex) {
                        return;
                    }
                    break;
                case "5":
                    //POP
                    id = command[1];

                    byte[] pop = rep.pop(id.getBytes());
                    p_rec = new DatagramPacket(pop, pop.length, currPeerAddress, SERVICE_PORT);
                    try {
                        s.send(p_rec);
                    } catch (IOException ex) {
                        return;
                    }
                    break;
                case "6":
                    //LIST
                    byte[] list = rep.list();
                    p_rec = new DatagramPacket(list, list.length, currPeerAddress, SERVICE_PORT);
                    try {
                        s.send(p_rec);
                    } catch (IOException ex) {
                        return;
                    }
                    break;
                default:
                    System.out.println("Reply: " + command[0]);
                    break;
            }
        }
    }

}
