/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Auth;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author 1170785
 */
public class UdpServer implements Runnable {

    private static final int SERVICE_PORT = /*9999*/ 30401;
    private static final String DEL = ";";
    private static final String ALGORITH_ERROR = "[ERROR]: No Available Algorithm!";

    private DatagramSocket s;

    public UdpServer(DatagramSocket udp_s) {
        s = udp_s;
    }

    public void run() {
        int i;
        byte[] data_rec = new byte[300], data_send = new byte[300];
        String frase, username, password;
        DatagramPacket p_rec, p_send;
        InetAddress currPeerAddress;
        p_rec = new DatagramPacket(data_rec, data_rec.length);
        p_send = new DatagramPacket(data_send, data_send.length);
        while (true) {
            try {
                s.receive(p_rec);
            } catch (IOException ex) {
                return;
            }
            currPeerAddress = p_rec.getAddress();
            frase = new String(p_rec.getData(), 0, p_rec.getLength());
            String command[] = frase.split(DEL);
            switch (command[0]) {
                case "1":
                    // peer start
                    UdpClient.addIP(p_rec.getAddress());
                    try {
                        s.send(p_rec);
                    } catch (IOException ex) {
                        return;
                    }
                    break;
                case "0":
                    // peer exit
                    UdpClient.remIP(p_rec.getAddress());
                    break;
                case "2":
                    //ADD ADDRESS WITH HOST
                    UdpClient.addIP(command[1], p_rec.getAddress().getHostAddress());
                    break;
                case "4":
                    //LOG IN
                    UserAuth userA = null;
                    try {
                        userA = ReadTxTFile.read();
                    } catch (FileNotFoundException ex) {
                        
                    }
                    username = command[1];
                    password = command[2];
                    String authToken = userA.getAuthToken(username);
                    String validationToken = "no token";
                    try {
                        validationToken = userA.md5(userA.md5(password).concat(authToken));
                    } catch (NoSuchAlgorithmException e) {
                        p_send = new DatagramPacket(ALGORITH_ERROR.getBytes(), 1, ALGORITH_ERROR.getBytes().length, currPeerAddress, SERVICE_PORT);
                        try {
                            s.send(p_send);
                        } catch (IOException ex) {
                            return;
                        }
                    }

                    String authenticate = "no authenticate";
                    try {
                        authenticate = userA.authenticate(username, validationToken);
                    } catch (NoSuchAlgorithmException e) {
                        p_send = new DatagramPacket(ALGORITH_ERROR.getBytes(), ALGORITH_ERROR.getBytes().length, currPeerAddress, SERVICE_PORT);
                        try {
                            s.send(p_send);
                        } catch (IOException ex) {
                            return;
                        }
                    }

                    p_send = new DatagramPacket(authenticate.getBytes(), authenticate.getBytes().length, currPeerAddress, SERVICE_PORT);
                    try {
                        s.send(p_send);
                    } catch (IOException ex) {
                        return;
                    }
                    break;
                default:
                    System.out.println("Reply: " + command[0]);
                    break;
            }
        }
    }
}
