/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Auth;

import java.util.Random;

/**
 *
 * @author 1170785
 */
public class AuthToken {

    private static final int MAX_STRING_LENGTH = 20;

    private String rand;
    private String username;

    public AuthToken(String username) {
        rand = generateRandomString();
        this.username = username;
    }

    /**
     * Generates a Random String
     *
     * @return
     */
    private String generateRandomString() {
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "0123456789" + "abcdefghijklmnopqrstuvxyz";

        // create StringBuffer size of AlphaNumericString 
        StringBuilder sb = new StringBuilder(MAX_STRING_LENGTH);

        for (int i = 0; i < MAX_STRING_LENGTH; i++) {
            // generate a random number between
            // 0 to AlphaNumericString variable length
            long currentTimeMillis = System.currentTimeMillis();
            int index = (int) (new Random(currentTimeMillis).nextInt(61));

            // add Character one by one in end of sb 
            sb.append(AlphaNumericString.charAt(index));
        }
        return sb.toString();
    }

    /**
     * Return token rand string
     *
     * @return rand string
     */
    public String getToken() {
        return this.rand;
    }

    /**
     * Return this token´s username
     *
     * @return username this token´s username
     */
    public String getUsername() {
        return this.username;
    }

}
