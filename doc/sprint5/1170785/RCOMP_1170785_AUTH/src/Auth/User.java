/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Auth;

/**
 *
 * @author 1170785
 */
public class User {

    private String username;
    private String password;

    /**
     * User Constructor
     *
     * @param username username of a user
     * @param passsword password of a user
     */
    public User(String username, String passsword) {
        this.username = username;
        this.password = passsword;
    }

    /**
     * Return as reponse the username
     *
     * @return the username
     */
    public String getUsername() {
        return this.username;
    }

    /**
     * Return as reponse the password
     *
     * @return the password
     */
    public String getPassword() {
        return this.password;
    }
}
