RCOMP 2018-2019 Project - Sprint 5 - Member 1170785 folder
===========================================
(This folder is to be created/edited by the team member 1170785 only)

The owner of this folder (member number 1170785) will commit here all the outcomes (results/artifacts/products) of his/her work during sprint 5. This may encompass any kind of standard file types.

#Explicação Adicional - UserAuthMD5

* This application manages a persistent list of usernames and corresponding passwords’ MD5 hashes. This list should be stored in a local file from where the application can read it when started. The main purpose is safe users’ authentication, here safe stands for never transferring through the network neither the password not the corresponding MD5 hash, both should be kept secret. The password is known by the user only, and de password’s MD hash is known by this application only. Of course by knowing the password, the user may calculate corresponding the MD5 hash.
	
* Hashes are fixed length sequences of bits, in the present case of MD5 it’s a 128-bit hash value. For the sake ok this application they should always be represented as text in hexadecimal notation, resulting in string of 32 hexadecimal digits. The authentication requires two steps, each with a request and a response.
	
* The main input command that is supported is the log in that uses this two methods:
	
	**1.** GetAuthToken - Given a username, the server returns as response a temporary one time random string to be used in the following authentication, called AuthToken. It will be valid only for the given username, and only for one single authentication. The application must guarantee  6/11 Redes de Computadores (RCOMP) – 2018/2019 AuthTokens are random, and as far as possible unique, suggestion: it could include a timestamp. The application must store the AuthToken together with the user account. Even if the provided username is unknown, an AuthToken should be provided as well, of course in this case the application will not store it.
	
	**2.** Authenticate - Given a username, and a generated validation hash, check if they match the user. The validation hash is calculated from the user provided password and the previously attained AuthToken. A suitable response message should be returned, e.g. either accepted or unaccepted. The unaccepted response encompasses all case of failure, including non-existent username, and invalid AuthToken, no additional hints should be provided about the reason. In any case the AuthToken is removed from the user account.


* The validation hash is calculated by: md5(concatenate(md5(password),AuthToken)).
	
* Where the password is the plain text user’s provided password. To check the received validation hash, the application calculates the its own version and compares both, it doesn’t know the password, but it knows md5(password). This is an adaptation of the well-known CHAP (Challenge-Handshake Authentication Protocol), of course, here the challenge is the AuthToken. 