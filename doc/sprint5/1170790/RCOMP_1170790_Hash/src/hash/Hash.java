/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hash;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author Daniel
 */
public class Hash {

    private static final String MD5_PROTOCOL_SECURITY = "md5";
    private static final String SHA0_PROTOCOL_SECURITY = "sha-0";
    private static final String SHA256_PROTOCOL_SECURITY = "sha-256";

    /**
     * Calculate a Hash value for a specific string with MD5
     *
     * @param toHash string to be converted
     * @return converted string using MD5 protocol
     * @throws NoSuchAlgorithmException
     */
    public static String md5(String toHash) throws NoSuchAlgorithmException {
        return digest(MD5_PROTOCOL_SECURITY, toHash);
    }

    /**
     * Given an identifier, representing the hash algorithm to be used, and a
     * text content to be used as input, it will calculate the corresponding
     * hash code and return it back as response. The response (hash code) is a
     * set of bits (actually the number of bits depends on the selected
     * algorithm), it must be provided in text format, by representing the hash
     * code in hexadecimal notation. If the requested algorithm is not
     * supported, a suitable error message should be retuned instead.
     *
     * @param type type of protocol that will be used
     * @param toHash string to be converted
     * @return converted string using protocol
     * @throws NoSuchAlgorithmException
     */
    public static String digest(String type, String toHash) throws NoSuchAlgorithmException {
        if (!type.equalsIgnoreCase(MD5_PROTOCOL_SECURITY) && !type.equalsIgnoreCase(SHA0_PROTOCOL_SECURITY) && !type.equalsIgnoreCase(SHA256_PROTOCOL_SECURITY)) {
            return "Hash type not supported";
        }
        MessageDigest md = MessageDigest.getInstance(type);
        md.update(toHash.getBytes());
        byte[] digest = md.digest();
        return DatatypeConverter.printHexBinary(digest).toUpperCase();
    }

    /**
     * Returns as response the list of identifiers for supported hash
     * algorithms.
     *
     * @return list of suported algorithms
     */
    public static String list() {
        return MD5_PROTOCOL_SECURITY + "," + SHA0_PROTOCOL_SECURITY + "," + SHA256_PROTOCOL_SECURITY;
    }

}
