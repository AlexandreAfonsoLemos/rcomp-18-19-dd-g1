/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hash;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.security.NoSuchAlgorithmException;
import java.util.List;

/**
 *
 * @author Daniel
 */
public class UdpServer implements Runnable {

    private DatagramSocket s;
    private static final int SERVICE_PORT = 30401;
    private static final String DEL = ";";
    private static final String ALGORITH_ERROR = "[ERROR]: No Available Algorithm";

    public UdpServer(DatagramSocket udp_s) {
        s = udp_s;
    }

    public void run() {
        int i;
        byte[] data_rec = new byte[300], data_send = new byte[300];
        String frase, id;
        DatagramPacket p_rec;
        DatagramPacket p_send;
        Hash rep = new Hash();
        InetAddress currPeerAddress;
        p_rec = new DatagramPacket(data_rec, data_rec.length);
        p_send = new DatagramPacket(data_send, data_send.length);
        while (true) {
            try {
                s.receive(p_rec);
            } catch (IOException ex) {
                return;
            }
            currPeerAddress = p_rec.getAddress();
            frase = new String(p_rec.getData(), 0, p_rec.getLength());
            String command[] = frase.split(DEL);
            switch (command[0]) {
                case "1":
                    // peer start
                    UdpClient.addIP(p_rec.getAddress());
                    try {
                        s.send(p_rec);
                    } catch (IOException ex) {
                        return;
                    }
                    break;
                case "0":
                    // peer exit
                    UdpClient.remIP(p_rec.getAddress());
                    break;
                case "2":
                    //ADD ADDRESS WITH HOST
                    UdpClient.addIP(command[1], p_rec.getAddress().getHostAddress());
                    break;
                case "3":
                    //RESPONCES TO VAR
                    break;
                case "4":
                    //DIGEST

                    String type = command[1];
                    String toHash = command[2];

                    String digest = "no digest";
                    try {
                        digest = Hash.digest(type, toHash);
                    } catch (NoSuchAlgorithmException ex) {
                        p_send = new DatagramPacket(ALGORITH_ERROR.getBytes(), ALGORITH_ERROR.getBytes().length, currPeerAddress, SERVICE_PORT);
                        try {
                            s.send(p_send);
                        } catch (IOException e) {
                            return;
                        }
                    }

                    p_rec = new DatagramPacket(digest.getBytes(), digest.getBytes().length, currPeerAddress, SERVICE_PORT);
                    try {
                        s.send(p_rec);
                    } catch (IOException ex) {
                        return;
                    }
                    break;
                case "5":
                    //LIST
                    String list = Hash.list();
                    p_rec = new DatagramPacket(list.getBytes(), list.getBytes().length, currPeerAddress, SERVICE_PORT);
                    try {
                        s.send(p_rec);
                    } catch (IOException ex) {
                        return;
                    }
                    break;
                default:
                    System.out.println("Reply: " + command[0]);
                    break;
            }
        }
    }

}
