RCOMP 2018-2019 Project - Sprint 5 - Member 1170790 folder
===========================================
(This folder is to be created/edited by the team member 1170790 only)

The owner of this folder (member number 1170790) will commit here all the outcomes (results/artifacts/products) of his/her work during sprint 5. This may encompass any kind of standard file types.

#Supplementary Explanation

##Hash Application Requirements
* This application is dedicated to calculate hash codes from provided data, is should be able to support different hash algorithms (e.g. MD5, SHA-0, SHA-256). 
Input commands to be supported are:

	1. **Digest** - Given an identifier, representing the hash algorithm to be used, and a text content to be used as input, it will calculate the corresponding hash code and return it back as response. The response (hash code) is a set of bits (actually the number of bits depends on the selected algorithm), it must be provided in text format, by representing the hash code in hexadecimal notation. If the requested algorithm is not supported, a suitable error message should be retuned instead.
	2. **List** - Returns as response the list of identifiers for supported hash algorithms.

