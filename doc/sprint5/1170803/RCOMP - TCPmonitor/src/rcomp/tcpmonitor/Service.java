/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rcomp.tcpmonitor;

/**
 *
 * @author Francisco
 */
public class Service {
    
    private String name;
    private String Ip_address;
    private int port_number;
    private String status;
    
    /**
     *
     * @param n
     * @param ip
     * @param p
     */
    public Service(String n, String ip, int p){
        setName(n);
        setIp_address(ip);
        setPort_number(p);
        setStatus("unchecked");
    }
    
    /**
     *
     * @param ip
     * @param port
     */
    public void changeService(String ip, int port){
        setIp_address(ip);
        setPort_number(port);
    }

    /**
     * @param name the name to set
     */
    private void setName(String name) {
        this.name = name;
    }

    /**
     * @param Ip_address the Ip_address to set
     */
    private void setIp_address(String Ip_address) {
        this.Ip_address = Ip_address;
    }

    /**
     * @param port_number the port_number to set
     */
    private void setPort_number(int port_number) {
        this.port_number = port_number;
    }
    
    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    
    /**
     * @return the Ip_address
     */
    public String getIp_address() {
        return Ip_address;
    }

    /**
     * @return the port_number
     */
    public int getPort_number() {
        return port_number;
    }

    @Override
    public String toString() {
        return name + "   " + getStatus() ;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }
    
}
