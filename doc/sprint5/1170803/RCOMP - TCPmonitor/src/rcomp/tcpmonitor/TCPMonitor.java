/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rcomp.tcpmonitor;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author Francisco
 */
public class TCPMonitor {

    List<Service> services = new ArrayList<>();

    public String add(String name, String ip, String portS){
        
        if(name.length() >= 80){
           return "The max length of name service is 80 char!!";
        }
         
        ip = verifyIP(ip);
        if(ip.length() > 20) return ip;
        int port = Integer.parseInt(portS);
        Service sv = new Service(name,ip,port);
        boolean ver=false;
        
        for(Service s : services){
            if(s.getName().equals(sv.getName())){
                s.changeService(sv.getIp_address(),sv.getPort_number());
                ver = true;
                return "Service changed with success!!";
            }
        }
        if(ver == false){
            services.add(sv);
            return "Service added with success!!";
        }
        return "Service added with success!!";
    }
    
    public String remove(String name){
        
        if(name.length() >= 80){
            return "The max length of name service is 80 char!!";
        }
        boolean ver=false;
        for(Service s : services){
            if(s.getName().equals(name)){
                services.remove(s);
                ver = true;
                return "Service removed with success!!";
            }
        }
        if(ver == false){
            return "Couldn´t remove the service because it doesn´t exist!!";
        }
        return "Couldn´t remove the service!!";
    }
    
    public String serviceStatus(){
        checkStatus();
        String serv, allServ="",allTest="";
        for(Service s : services){
            serv = s.toString();
            allTest = allTest + "\n" + serv;
            if(allTest.getBytes().length > 476){
                allServ = allServ + "\n" + "Max message size reached";
                break;
            } else {
                allServ = allServ + "\n" +  serv;
            }
        }
        
        return allServ;
    }
    
    public String verifyIP(String ip){
        
        String[] temp = ip.split("\\.");
        for(int i=0;i<4;i++){
            int num = Integer.parseInt(temp[i]);
            if(num < 0 || num > 255){
                return "Introduce the IP address in a correct format (0-255.0-255.0-255.0-255)";
            }
        }
        return ip;
    }
    
    public void checkStatus(){
        
        Random rand = new Random();
        for(Service s : services){
            if(!s.getStatus().equals("unchecked")){
                int n = 1 + rand.nextInt(2);
                if(n == 1){
                    s.setStatus("available");
                }
                if(n == 2){
                    s.setStatus("unavailable");
                }
            }
        }
    }
    
}   

