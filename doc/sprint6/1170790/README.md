RCOMP 2018-2019 Project - Sprint 6 - Member 1170790 folder
===========================================
(This folder is to be created/edited by the team member 1170790 only)

The owner of this folder (member number 1170790) will commit here all the outcomes (results/artifacts/products) of his/her work during sprint 6. This may encompass any kind of standard file types.

* My html page shows the user the application menu and allows the him to introduce the options through a message bar, which in turn are received by the java script that uses AJAX to retrieve the message to our server. 

* Then our server uses the same service to relay the answer back to the html in real time. For this to be achievable we used the DEI servers to host ours and when opening the html in the university lan a connection between the two is possible. 

* The available functions in our application are the following:

	1. **Digest** - Given an identifier, representing the hash algorithm to be used, and a text content to be used as input, it will calculate the corresponding hash code and return it back as response. The response (hash code) is a set of bits (actually the number of bits depends on the selected algorithm), it must be provided in text format, by representing the hash code in hexadecimal notation. If the requested algorithm is not supported, a suitable error message should be retuned instead.
	
	2. **List** - Returns as response the list of identifiers for supported hash algorithms.
