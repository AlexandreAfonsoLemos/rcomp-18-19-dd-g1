package hash;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.security.NoSuchAlgorithmException;

/**
 * UDPServer Euquals
 *
 */
public class HTTPReq {

    private String baseFolder;
    private Socket sock;
    private DataInputStream inS;
    private DataOutputStream outS;

    public HTTPReq(Socket clientSock, String baseFolder) {
        this.sock = clientSock;
        this.baseFolder = baseFolder;
    }

    public void start() throws NoSuchAlgorithmException {
        try {
            outS = new DataOutputStream(sock.getOutputStream());
            inS = new DataInputStream(sock.getInputStream());
        } catch (IOException ex) {
            System.out.println("Thread error on data streams creation");
        }
        try {
            HTTPmessage request = new HTTPmessage(inS);
            HTTPmessage response = new HTTPmessage();

            if (request.getMethod().equals("GET")) {
                if (request.getURI().startsWith("/hash")) {
                    String[] splitUri = request.getURI().split("/");
                    String html = "";
                    response.setContentFromString(html, "text/html");
                } else {
                    String fullname = baseFolder;
                    if (request.getURI().equals("/")) {
                        fullname = fullname + "index.html";
                    } else {
                        fullname = fullname + request.getURI();
                    }
                    if (response.setContentFromFile(fullname)) {
                        response.setResponseStatus("200 Ok");
                    } else {
                        response.setContentFromString(
                                "<html><body><h1>404 File not found</h1></body></html>",
                                "text/html");
                        response.setResponseStatus("404 Not Found");
                    }
                }
                response.send(outS);
            } else if (request.getMethod().equals("PUT")) {
                if (request.getURI().startsWith("/hash/")) {
                    String arrSplit[] = request.getURI().split("/");
                    if (arrSplit[2].equals("digest")) {
                        response.setResponseStatus("200 ok");
                        try {
                            String digest = Hash.digest(arrSplit[3], arrSplit[4]);
                            response.setContentFromString("Hash: " + digest, "text/html");
                        } catch (NoSuchAlgorithmException e) {
                            response.setContentFromString("404 No Algorithm was found",
                                    "text/html");
                        }
                    } else if (arrSplit[2].equals("list")) {
                        response.setResponseStatus("200 ok");
                        response.setContentFromString("Hash: " + Hash.list(), "text");
                    }
                    response.send(outS);
                }
            }
        } catch (IOException ex) {
            System.out.println("Thread error when reading request");
        }
        try {
            sock.close();
        } catch (IOException ex) {
            System.out.println("CLOSE IOException");
        }
    }
}
