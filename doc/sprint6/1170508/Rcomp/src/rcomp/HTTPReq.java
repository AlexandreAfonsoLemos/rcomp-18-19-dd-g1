/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rcomp;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

/**
 *
 * @author muril
 */

public class HTTPReq {

    private String baseFolder;
    private Socket sock;
    private DataInputStream inS;
    private DataOutputStream outS;
    private Walls walls;

    public HTTPReq(Socket clientSock, Walls walls, String baseFolder) {
        this.sock = clientSock;
        this.walls = walls;
        this.baseFolder = baseFolder;
    }

    public void start() {
        try {
            outS = new DataOutputStream(sock.getOutputStream());
            inS = new DataInputStream(sock.getInputStream());
        } catch (IOException ex) {
            System.out.println("Thread error on data streams creation");
        }
        try {
            HTTPmessage request = new HTTPmessage(inS);
            HTTPmessage response = new HTTPmessage();
            response.setResponseStatus("200 ok");

            if (request.getMethod().equals("GET")) {
                if (request.getURI().startsWith("/walls/")) {
                    String[] splitUri = request.getURI().split("/");
                    String html = "";
                    if (splitUri.length > 2) {
                        html = walls.wallToHtml(splitUri[2]);
                    }
                    response.setContentFromString(html, "text/html");
                } else {
                    String fullname = baseFolder;
                    if (request.getURI().equals("/")) {
                        fullname = fullname + "index.html";
                    } else {
                        fullname = fullname + request.getURI();
                    }
                    if (response.setContentFromFile(fullname)) {
                        response.setResponseStatus("200 Ok");
                    } else {
                        response.setContentFromString(
                                "<html><body><h1>404 File not found</h1></body></html>",
                                "text/html");
                        response.setResponseStatus("404 Not Found");
                    }
                }
                response.send(outS);
            } else if (request.getMethod().equals("POST")) {
                if (request.getURI().startsWith("/walls/")) {
                    String[] splitUri = request.getURI().split("/");
                    if (splitUri.length > 2) {
                        walls.addMessage(splitUri[2], splitUri[3]);
                        String html = walls.wallToHtml(splitUri[2]);
                        response.setContentFromString(html, "text/html");
                    } else {
                        walls.deleteWall(splitUri[2]);
                        String html = walls.wallToHtml(splitUri[2]);
                        response.setContentFromString(html, "text/html");
                    }
                } else {
                    response.setContentFromString(
                            "<html><body><h1>ERROR: 405 Method Not Allowed</h1></body></html>",
                            "text/html");
                    response.setResponseStatus("405 Method Not Allowed");
                }
                response.send(outS);
            } else if (request.getMethod().equals("DELETE")) {
                String[] splitUri = request.getURI().split("/");
                if (splitUri.length > 3) {
                    walls.deleteMessage(splitUri[2], Integer.parseInt(splitUri[3]));
                    String html = walls.wallToHtml(splitUri[2]);
                    response.setContentFromString(html, "text/html");
                } else {
                    walls.deleteWall(splitUri[2]);
                    String html = walls.wallToHtml(splitUri[2]);
                    response.setContentFromString(html, "text/html");
                }
                response.send(outS);
            }
        } catch (IOException ex) {
            System.out.println("Thread error when reading request");
        }
        try {
            sock.close();
        } catch (IOException ex) {
            System.out.println("CLOSE IOException");
        }
    }
}

