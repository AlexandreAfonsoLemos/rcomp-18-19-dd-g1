/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rcomp;

/**
 *
 * @author muril
 */
public class NumConvert {
    
    private final int DECIMAL = 10;
    private final int HEXADECIMAL = 16;
    private final int OCTAL = 8;
    private final int BINARY = 2;
    
    
    public String convert(String IR, String num, String OR) {
        int i = this.getBase(IR);
        int o = this.getBase(OR);
        int ret;
        if(i ==0) {
            return "Impossible to convert, unknown input representation";
        }
        if(o==0) {
            return "Impossible to convert, unknown ouput representation";
        }
        if (i == o) {
            return num;
        }
//        try{
        if (o != DECIMAL) {
            ret = toDecimal(num, i);
            return decTo(ret, o);
        } else {
            return String.valueOf(toDecimal(num, i)).toUpperCase();
        }
//        }catch(NumberFormatException e){
//            return "Impossible to convert";
//        }
    }
    
    private int toDecimal(String n, int rep) {
        if (rep == OCTAL) {
            return Integer.parseInt(n, 8);
        }
        if (rep == HEXADECIMAL) {
            return Integer.parseInt(n, 16);
        }
        if (rep == BINARY) {
            return Integer.parseInt(n, BINARY);
        }
        return Integer.parseInt(n);
    }
    
    private String decTo(int num, int rep) {
        if (rep == HEXADECIMAL) {
            return Integer.toHexString(num).toUpperCase();
        } else {
            if (rep == BINARY) {
                return Integer.toBinaryString(num).toUpperCase();
            } else {
                if (rep == OCTAL) {
                    return Integer.toOctalString(num).toUpperCase();
                }
            }
        }
        return String.valueOf(num).toUpperCase();
    }
    
    private int getBase(String rep) {
        if (rep.equals("BIN")) {
            return BINARY;
        }
        if (rep.equals("OCT")) {
            return OCTAL;
        }
        if (rep.equals("HEX")) {
            return HEXADECIMAL;
        }
        if (rep.equals("DEC")) {
            return DECIMAL;
        }
        return 0;
    }
    
    public String list(){
        return  "Representations\n"+
                "DECIMAL(DEC)\n"+
                "HEXADECIMAL(HEX)\n"+
                "OCTAL(OCT)\n"+
                "BINARY(BIN)\n";  
    }
}
