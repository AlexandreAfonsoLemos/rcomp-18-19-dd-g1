/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rcomp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 *
 * @author muril
 */
public class UdpServer implements Runnable {

    private static final int SERVICE_PORT = 30401;
    private static final String DEL = ";";
    private static final String CONVERSION_ERROR = "[ERROR]: impossible convertion";

    private DatagramSocket s;

    public UdpServer(DatagramSocket udp_s) {
        s = udp_s;
    }

    public void run() {
        int i;
        byte[] data = new byte[300];
        String frase, username, password;
        DatagramPacket p_rec, p_send;
        InetAddress currPeerAddress;
        NumConvert convert = new NumConvert();
        p_rec = new DatagramPacket(data, data.length);
        while (true) {
            try {
                s.receive(p_rec);
            } catch (IOException ex) {
                return;
            }
            currPeerAddress = p_rec.getAddress();
            frase = new String(p_rec.getData(), 0, p_rec.getLength());
            String command[] = frase.split(DEL);
            switch (command[0]) {
                case "1":
                    // peer start
                    UdpClient.addIP(p_rec.getAddress());
                    try {
                        s.send(p_rec);
                    } catch (IOException ex) {
                        return;
                    }
                    break;
                case "0":
                    // peer exit
                    UdpClient.remIP(p_rec.getAddress());
                    break;
                case "2":
                    //ADD ADDRESS WITH HOST
                    UdpClient.addIP(command[1], p_rec.getAddress().getHostAddress());
                    break;
                case "3":
                    //RESPONCES TO AUTH
                    String reply[] = command[1].split(DEL);
                    if (reply.length == 1) {
                        System.out.println(command[1]);
                    } else {
                        for (i = 0; i < reply.length; i++) {
                            System.out.println(reply[i]);
                        }
                    }
                    break;
                case "4":
                    //NUM CONVERT
                    String convertido = "NUM";
                    try {
                        String IR = command[1];
                        String NUM = command[2];
                        String OR = command[3];

                        convertido = convert.convert(IR, NUM, OR);
                    } catch (NumberFormatException | ArrayIndexOutOfBoundsException e1) {
                        p_send = new DatagramPacket(CONVERSION_ERROR.getBytes(), CONVERSION_ERROR.getBytes().length, currPeerAddress, SERVICE_PORT);
                        try {
                            s.send(p_rec);
                        } catch (IOException ex) {
                            return;
                        }
                    }

                    p_send = new DatagramPacket(convertido.getBytes(), convertido.getBytes().length, currPeerAddress, SERVICE_PORT);
                    try {
                        s.send(p_send);
                    } catch (IOException ex) {
                        return;
                    }

                    break;

                case "5":
                    String lista = convert.list();

                    p_send = new DatagramPacket(lista.getBytes(), lista.getBytes().length, currPeerAddress, SERVICE_PORT);
                    try {
                        s.send(p_send);
                    } catch (IOException ex) {
                        return;
                    }
                    break;
                default:
                    System.out.println("Reply: " + command[0]);
                    break;
            }
        }
    }
}
