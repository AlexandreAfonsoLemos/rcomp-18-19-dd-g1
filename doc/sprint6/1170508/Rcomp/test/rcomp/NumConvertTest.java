/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rcomp;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author muril
 */
public class NumConvertTest {

    public NumConvertTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of convert method, of class NumConvert.
     */
    @Test
    public void testConvert() {
        System.out.println("convert");
        NumConvert instance = new NumConvert();

        
        assertEquals("30AC", instance.convert("HEX", "30AC", "HEX"));
        assertEquals("12460", instance.convert("HEX", "30AC", "DEC"));
        assertEquals("30254", instance.convert("HEX", "30AC", "OCT"));
        assertEquals("11000010101100", instance.convert("HEX", "30AC", "BIN"));

        assertEquals("2D", instance.convert("DEC", "45", "HEX"));
        assertEquals("45", instance.convert("DEC", "45", "DEC"));
        assertEquals("55", instance.convert("DEC", "45", "OCT"));
        assertEquals("101101", instance.convert("DEC", "45", "BIN"));

        assertEquals("4E4", instance.convert("OCT", "2344", "HEX"));
        assertEquals("1252", instance.convert("OCT", "2344", "DEC")); 
        assertEquals("2344", instance.convert("OCT", "2344", "OCT"));
        assertEquals("10011100100", instance.convert("OCT", "2344", "BIN"));

        assertEquals("8F", instance.convert("BIN", "10001111", "HEX"));
        assertEquals("143", instance.convert("BIN", "10001111", "DEC"));
        assertEquals("217", instance.convert("BIN", "10001111", "OCT"));
        assertEquals("10001111", instance.convert("BIN", "10001111", "BIN"));
        
        assertEquals("Impossible to convert, unknown input representation", instance.convert("HEd", "3", "BIN"));
        assertEquals("Impossible to convert, unknown ouput representation", instance.convert("HEX", "3", "NI"));
    }
}
