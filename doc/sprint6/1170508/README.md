RCOMP 2018-2019 Project - Sprint 6 - Member 1170508 folder
===========================================
(This folder is to be created/edited by the team member 1170508 only)


# Aditional explanation

- The main html page shows the user the application menu and allows the him to introduce the options through a message bar, which in turn are received by the java script that uses AJAX to retrieve the message to our server.

- Our server uses the same service to relay the answer back to the html in real time. For this to
be achievable we used the DEI servers to host ours and when opening the html in the university lan a connection between the two is possible.

- NumConvert page shows the user 3 message bars to insert data. The current representation of the number, the number to convert, and the representation that the user wants to see the number converted to. Also show's a button that lists the representations available.
