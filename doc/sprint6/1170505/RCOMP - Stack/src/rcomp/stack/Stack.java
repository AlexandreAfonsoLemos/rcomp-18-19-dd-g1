/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rcomp.stack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Alex
 */
public class Stack {

    private byte stackIdentifier[];
    private List<byte[]> pile;

    public Stack(byte[] name) {
        this.stackIdentifier = name;
        this.pile = new ArrayList<>();
    }

    public int addContentToStack(byte[] content) {
        pile.add(0, content);
        return pile.size();
    }

    public byte[] getStackIdentifier() {
        return stackIdentifier;
    }

    public List<byte[]> getPile() {
        return pile;
    }

    public byte[] removeContent() {
        return pile.remove(0);
    }

    public boolean isEmpty() {
        return pile.isEmpty();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Stack other = (Stack) obj;
        return Arrays.equals(this.stackIdentifier, other.stackIdentifier);
    }

}
