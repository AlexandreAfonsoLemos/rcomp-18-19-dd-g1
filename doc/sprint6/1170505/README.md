RCOMP 2018-2019 Project - Sprint 6 - Member 1170505 folder
===========================================
(This folder is to be created/edited by the team member 1170505 only)

The owner of this folder (member number 1170505) will commit here all the outcomes (results/artifacts/products) of his/her work during sprint 6. This may encompass any kind of standard file types.

# Explicação Adiconal

* My html page shows the user the application menu and allows the him to introduce the options through a message bar, which in turn are received by the java script that uses AJAX to retrieve the message to our server.

* Then our server uses the same service to relay the answer back to the html in real time. For this to be achievable we used the DEI servers to host ours and when opening the html in the university lan a connection between the two is possible.

* The available functions in our application are the following:

* The main input command that is supported is the log in that uses this 3 methods:

	**1.** Push - Given a stack identifier and a text content, stores it in that stack (top of the pile). If the identifier doesn ́t exist, a new stack is created. As response,returns a number representing the stack size (number of elements)after thepush operation.

	**2.** Pop - Given a stack identifier, returns the top element (text content) as response message, and removes it from the stack. If no elements are left, the stack is removed. If the identifier doesn ́t exist, a suitable error message is returned.

  **3.** Returns,as  response,the  list  of  current  stack  identifiers,  and  the  number  of  elements present in each.
