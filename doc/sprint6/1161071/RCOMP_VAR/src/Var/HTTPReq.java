package Var;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.security.NoSuchAlgorithmException;

/**
 * UDPServer Euquals
 *
 */
public class HTTPReq {

    private String baseFolder;
    private Socket sock;
    private DataInputStream inS;
    private DataOutputStream outS;
    private VarRepository rep;

    public HTTPReq(Socket clientSock, VarRepository rep, String baseFolder) {
        this.sock = clientSock;
        this.baseFolder = baseFolder;
        this.rep = new VarRepository();
    }

    public void start() throws NoSuchAlgorithmException {
        try {
            outS = new DataOutputStream(sock.getOutputStream());
            inS = new DataInputStream(sock.getInputStream());
        } catch (IOException ex) {
            System.out.println("Thread error on data streams creation");
        }
        try {
            HTTPmessage request = new HTTPmessage(inS);
            HTTPmessage response = new HTTPmessage();

            if (request.getMethod().equals("GET")) {
                if (request.getURI().startsWith("/var")) {
                    String[] splitUri = request.getURI().split("/");
                    String html = "";
                    response.setContentFromString(html, "text/html");
                } else {
                    String fullname = baseFolder;
                    if (request.getURI().equals("/")) {
                        fullname = fullname + "index.html";
                    } else {
                        fullname = fullname + request.getURI();
                    }
                    if (response.setContentFromFile(fullname)) {
                        response.setResponseStatus("200 Ok");
                    } else {
                        response.setContentFromString(
                                "<html><body><h1>404 File not found</h1></body></html>",
                                "text/html");
                        response.setResponseStatus("404 Not Found");
                    }
                }
                response.send(outS);
            } else if (request.getMethod().equals("PUT")) {
                if (request.getURI().startsWith("/var/")) {
                    String arrSplit[] = request.getURI().split("/");
                    if (arrSplit[2].equals("store")) {
                        response.setResponseStatus("200 ok");
                        byte[] store = rep.store(arrSplit[3].getBytes(), arrSplit[4].getBytes());
                        String string = new String(store, 0, store.length);
                        response.setContentFromString("Var: " + string, "text/html");
                    } else if (arrSplit[2].equals("list")) {
                        response.setResponseStatus("200 ok");
                        String list = rep.list(Integer.parseInt(arrSplit[3]));
                        response.setContentFromString("Var: " + list, "text/html");
                    } else if (arrSplit[2].equals("fetch")) {
                        response.setResponseStatus("200 ok");
                        byte[] fetch = rep.fetch(arrSplit[3].getBytes());
                        String string = new String(fetch, 0, fetch.length);
                        response.setContentFromString("Var: " + string, "text/html");
                    } else if (arrSplit[2].equals("erase")) {
                        response.setResponseStatus("200 ok");
                        byte[] erase = rep.erase(arrSplit[3].getBytes());
                        String string = new String(erase, 0, erase.length);
                        response.setContentFromString("Var: " + string, "text/html");
                    }
                    response.send(outS);
                }
            }
        } catch (IOException ex) {
            System.out.println("Thread error when reading request");
        }
        try {
            sock.close();
        } catch (IOException ex) {
            System.out.println("CLOSE IOException");
        }
    }
}
