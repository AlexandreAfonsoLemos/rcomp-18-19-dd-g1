/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Var;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author 1161071
 */
public class VarRepository {

    private static final String MSG_TRUNCATE = "List Truncated;";
    private static final String MSG_ERROR = "Identifier not found!";
    private static final String MSG_ERROR_STORE = "Invalid content!";
    private static final String DELIMITER = ",";

    private List<Var> varList;

    public VarRepository() {
        varList = new ArrayList<Var>();
    }

    /**
     * Given an identifier and a text content, stores it. As response, the same
     * text content should be returned. If the identifier already exists, then
     * its content is overwritten. If the content is empty, then the identifier
     * is removed.
     *
     * @param name identifier
     * @param content message to store
     * @return content
     */
    public byte[] store(byte[] name, byte[] content) {
        int i;
        if (content.length == 0 || content == null) { //check if content is null or empty
            erase(name);
            return MSG_ERROR_STORE.getBytes();
        } else {
            for (i = 0; i < varList.size(); i++) { //search for equal Var by name being equal
                if (Arrays.equals(varList.get(i).getName(), name)) {
                    Var var_equal = varList.get(i);
                    var_equal.setContent(content);
                    varList.add(i, var_equal);
                    return content;
                }
            }
            varList.add(new Var(name, content));
            return content;
        }
    }

    /**
     * Given an identifier, fetches its content. As response, the stored text
     * content should be returned. If the identifier doesn’t exist an error
     * message should be returned.
     *
     * @param name identifier
     * @return content for identifier
     */
    public byte[] fetch(byte[] name) {
        for (Var var : varList) {
            if (var.equals(new Var(name, name))) {
                return var.getContent();
            }
        }
        return MSG_ERROR.getBytes();
    }

    /**
     * As response, a list of identifiers should be provided. The list should be
     * truncated if it exceeds the maximum message size, in that case there
     * should be a remark about that in the response message.
     *
     * @param message_size size message required to exchange
     * @return String with name identifiers delimiter ";"
     */
    public String list(int message_size) {
        int lengs = 0;
        String message = "";
        for (Var var : varList) {
            lengs = lengs + var.getName().length + 1;
        }

        if (lengs > message_size) {
            int i = 0;
            while (message.length() + varList.get(i).getName().length < message_size - MSG_TRUNCATE.length()) {
                message = message + new String(varList.get(i).getName(), 0, varList.get(i).getName().length) + DELIMITER;
                i++;
            }
            message = message + MSG_TRUNCATE;
        } else {
            for (Var var : varList) {
                message = message + new String(var.getName(), 0, var.getName().length) + DELIMITER;
            }
        }
        return message;
    }

    /**
     * Given an identifier, fetches its content and removes the
     * identifier/content. As response, the stored text content should be
     * returned. If the identifier doesn’t exist an error message should be
     * returned.
     *
     * @param name identifier
     * @return content errased
     */
    public byte[] erase(byte[] name) {
        byte[] content = fetch(name);
        for (Var var : varList) {
            if (Arrays.equals(var.getName(), name)) {
                varList.remove(var);
                return content;
            }
        }
        return MSG_ERROR.getBytes();
    }

}
