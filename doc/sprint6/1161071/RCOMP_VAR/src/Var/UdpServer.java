/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Var;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 *
 * @author Daniel
 */
public class UdpServer implements Runnable {

    private static final int SERVICE_PORT = 30401;
    private static final String DEL = ";";

    private DatagramSocket s;

    public UdpServer(DatagramSocket udp_s) {
        s = udp_s;
    }

    public void run() {
        int i;
        byte[] data_rec = new byte[300], data_send = new byte[300];
        String frase, id;
        DatagramPacket p_rec, p_send;
        VarRepository rep = new VarRepository();
        InetAddress currPeerAddress;
        p_rec = new DatagramPacket(data_rec, data_rec.length);
        p_send = new DatagramPacket(data_send, data_send.length);
        while (true) {
            try {
                s.receive(p_rec);
            } catch (IOException ex) {
                return;
            }
            currPeerAddress = p_rec.getAddress();
            frase = new String(p_rec.getData(), 0, p_rec.getLength());
            String command[] = frase.split(DEL);
            switch (command[0]) {
                case "1":
                    // peer start
                    UdpClient.addIP(p_rec.getAddress());
//                    p_send.setAddress(currPeerAddress);
//                    data_send = data_rec;
//                    p_send.setData(data_send);
//                    p_send.setLength(data_send.length);
                    try {
                        s.send(p_rec);
                    } catch (IOException ex) {
                        return;
                    }
                    break;
                case "0":
                    // peer exit
                    UdpClient.remIP(p_rec.getAddress());
                    break;
                case "2":
                    //ADD ADDRESS WITH HOST
                    UdpClient.addIP(command[1], p_rec.getAddress().getHostAddress());
                    break;
                case "3":
                    //RESPONCES TO VAR
                    break;
                case "4":
                    //STORE
                    id = command[1];
                    String content = command[2];
                    byte[] store = rep.store(id.getBytes(), content.getBytes());
                    DatagramPacket p_send_0 = new DatagramPacket(store, store.length, currPeerAddress, SERVICE_PORT);
                    try {
                        s.send(p_send_0);
                    } catch (IOException ex) {
                        return;
                    }
                    break;
                case "5":
                    //FETCH
                    id = command[1];
                    byte[] fetch = rep.fetch(id.getBytes());
                    DatagramPacket p_send_1 = new DatagramPacket(fetch, fetch.length, currPeerAddress, SERVICE_PORT);
                    try {
                        s.send(p_send_1);
                    } catch (IOException ex) {
                        return;
                    }
                    break;
                case "6":
                    //LIST
                    int size = Integer.parseInt(command[1]);
                    String list = rep.list(size);
                    DatagramPacket p_send_2 = new DatagramPacket(list.getBytes(), list.getBytes().length, currPeerAddress, SERVICE_PORT);
                    try {
                        s.send(p_send_2);
                    } catch (IOException ex) {
                        return;
                    }
                    break;
                case "7":
                    //ERASE
                    id = command[1];
                    byte[] erase = rep.erase(id.getBytes());
                    DatagramPacket p_send_3 = new DatagramPacket(erase, erase.length, currPeerAddress, SERVICE_PORT);
                    try {
                        s.send(p_send_3);
                    } catch (IOException ex) {
                        return;
                    }
                    break;
                default:
                    System.out.println("Reply: " + command[0]);
                    break;
            }
        }
    }

}
