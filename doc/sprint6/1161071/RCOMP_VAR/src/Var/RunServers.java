package Var;

import java.io.IOException;

/**
 *
 */
public class RunServers {
    
    private static VarRepository repV;

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        int port1 = 30401;
        System.out.println("Listening for requests (HTTP, IPv4 and IPv6)..");
        repV = new VarRepository();
        new Thread(new HTTPServ(port1, repV)).start();
    }
}
