package Var;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.NoSuchAlgorithmException;

/**
 * MAIN
 */
public class HTTPServ implements Runnable {

    private final String BASE_FOLDER = "www/";
    private ServerSocket sock;
    private VarRepository repV;
    private int port;

    public HTTPServ(int port, VarRepository va) {
        this.port = port;
        this.repV = va;
    }

    @Override
    public void run() {
        Socket cliSock;
        try {
            sock = new ServerSocket(port);
        } catch (IOException e) {
            System.out.println("Couldn't load local port " + port);
            System.exit(1);
        }
        while (true) {
            try {
                cliSock = sock.accept();
                HTTPReq req = new HTTPReq(cliSock, repV, BASE_FOLDER);
                req.start();
            } catch (IOException ex) {
                System.out.println("Error on socket accept!");
            } catch (NoSuchAlgorithmException ex) {

            }
        }
    }
}
