RCOMP 2018-2019 Project - Sprint 6 - Member 1161071 folder
===========================================
(This folder is to be created/edited by the team member 1161071 only)

The owner of this folder (member number 1161071) will commit here all the outcomes (results/artifacts/products) of his/her work during sprint 6. This may encompass any kind of standard file types.

# Explicação Adiconal

* My html page shows the user the application menu and allows the him to introduce the options through a message bar, which in turn are received by the java script that uses AJAX to retrieve the message to our server. 

* Then our server uses the same service to relay the answer back to the html in real time. For this to be achievable we used the DEI servers to host ours and when opening the html in the university lan a connection between the two is possible. 

* The available functions in our application are the following:

* Each text content to be stored is identified by a name (up to 80 characters long, case sensitive), the input commands to be supported are:

	**1.** Store - Given an identifier and a text content, stores it. As response, the same text content should be returned. If the identifier already exists, then its content is overwritten. If the content is empty, then the identifier is removed.
	
	**2.** Fetch - Given an identifier, fetches its content. As response, the stored text content should be returned. If the identifier doesn’t exist an error message should be returned.
	
	**3.** List - As response, a list of identifiers should be provided. The list should be truncated if it exceeds the maximum message size, in that case there should be a remark about that in the response message.
	
	**4.** Erase - Given an identifier, fetches its content and removes the identifier/content. As response, the stored text content should be returned. If the identifier doesn’t exist an error message should be returned