/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Auth;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author 1170785
 */
public class UserAuth {

    private List<AuthToken> list_tokens;
    private List<User> list_users;

    public UserAuth() {
        list_tokens = new ArrayList<>();
        list_users = new ArrayList<>();
    }

    /**
     * Given a username, the server returns as response a temporary one time
     * random string to be used in the following authentication, called
     * AuthToken. It will be valid only for the given username, and only for one
     * single authentication. The application must guarantee AuthTokens are
     * random, and as far as possible unique, suggestion: it could include a
     * timestamp. The application must store the AuthToken together with the
     * user account. Even if the provided username is unknown, an AuthToken
     * should be provided as well, of course in this case the application will
     * not store it
     *
     * @param username
     * @return AuthToken
     */
    public String getAuthToken(String username) {
        AuthToken newAuthToken = new AuthToken(username);
        for (AuthToken token : list_tokens) {
            if (username.equals(token.getUsername())) {
                return newAuthToken.getToken();
            }
        }
        for (User user : list_users) {
            if (user.getUsername().equals(username)) {
                list_tokens.add(newAuthToken);
                return newAuthToken.getToken();
            }
        }
        return newAuthToken.getToken();
    }

    /**
     * Given a username, and a generated validation hash, check if they match
     * the user. The validation hash is calculated from the user provided
     * password and the previously attained AuthToken. A suitable response
     * message should be returned, e.g. either accepted or unaccepted. The
     * unaccepted response encompasses all case of failure, including
     * non-existent username, and invalid AuthToken, no additional hints should
     * be provided about the reason. In any case the AuthToken is removed from
     * the user account.
     *
     * @param username username of the user
     * @param validationHash
     * @return the result of the operation
     * @throws NoSuchAlgorithmException
     */
    public String authenticate(String username, String validationHash) throws NoSuchAlgorithmException {

        String password = "";
        for (User user : list_users) {
            if (user.getUsername().equalsIgnoreCase(username)) {
                password = user.getPassword();
            }
        }
        if (password.isEmpty()) {
            return "Unaccepted: User not found!";
        }

        AuthToken authtoken = null;
        for (AuthToken token : list_tokens) {
            if (token.getUsername().equalsIgnoreCase(username)) {
                authtoken = token;
            }
        }

        if (authtoken == null) {
            return "Unaccepted: Autorization failed!";
        }

        String newHash = md5(password.concat(authtoken.getToken()));

        if (validationHash.equalsIgnoreCase(newHash)) {
            list_tokens.remove(authtoken);
            return "Accepted: Welcome " + username + "!!";
        } else {
            list_tokens.remove(authtoken);
            return "Unaccepted: Password is wrong!";
        }
    }

    public String md5(String toHash) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(toHash.getBytes());
        byte[] digest = md.digest();
        return DatatypeConverter.printHexBinary(digest).toUpperCase();
    }

    public void addUser(String username, String password) {
        User user = new User(username, password);
        list_users.add(user);
    }
}
