/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Auth;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author 1170785
 */
public class Walls {
    
     private HashMap<String, ArrayList<String>> walls;

    public Walls() {
        this.walls = new HashMap<>();
    }

    public synchronized ArrayList<String> getWall(String wallName) {
        if (!walls.containsKey(wallName)) {
            createWall(wallName);
        }
        return walls.get(wallName);
    }

    public synchronized void createWall(String wallName) {
        walls.put(wallName, new ArrayList<>());
    }

    public synchronized String wallToHtml(String wallName) {
        int msg = 0;
        ArrayList<String> messages = getWall(wallName);
        String html = new String();
        if (messages.isEmpty()) {
            html = "";
        } else {
            for (String s : messages) {
                String temp = new String();
                String[] split = s.split("%20");
                if (split.length > 1) {
                    for (int i = 0; i < split.length; i++) {
                        temp = temp + split[i] + " ";
                    }
                    html = html + "[" + msg + "] " + temp + "\n";
                } else {
                    html = html + "[" + msg + "] " + s + "\n";
                }
                msg++;
            }
        }
        return html;
    }

    public synchronized void addMessage(String wall, String message) {
        String[] messageSplit = message.split(":");
        //messageSplit[1] = messageSplit[1].substring(3);
        String newMessage = messageSplit[0] + ": " + messageSplit[1];
        ArrayList<String> msgs = walls.get(wall);
        msgs.add(newMessage);
        walls.put(wall, msgs);
    }

    public synchronized void deleteMessage(String wall, int msgNum) {
        ArrayList<String> msgs = walls.get(wall);
        msgs.remove(msgNum);
        walls.put(wall, msgs);
    }
    
    public synchronized void deleteWall(String wall){
        walls.remove(wall);
    }
}
