/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Auth;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 *
 * @author 1161071
 */
public class UdpClient {

    private static HashSet<InetAddress> peersList = new HashSet<>();

    private static Map<String, String> apps_ip = new HashMap<String, String>();

    public static synchronized void addIP(InetAddress ip) {
        peersList.add(ip);
    }

    public static void addIP(String id, String hostAddress) {
        apps_ip.put(id, hostAddress);
    }

    public static synchronized void remIP(InetAddress ip) {
        peersList.remove(ip);
    }

    public static synchronized void printIPs() {
        for (InetAddress ip : peersList) {
            System.out.print(" " + ip.getHostAddress());
        }
    }

    public static synchronized void addAllIPs() {
        apps_ip.put("VAR", "172.18.154.202");
        apps_ip.put("HASH", "172.18.155.244");
        apps_ip.put("STACK", "172.18.155.213");
        apps_ip.put("TCP", "172.18.154.82");
        apps_ip.put("CONVERT", "172.18.152.100");
    }

    public static synchronized void sendToAll(DatagramSocket s, DatagramPacket p) throws Exception {
        for (InetAddress ip : peersList) {
            p.setAddress(ip);
            s.send(p);
        }
    }

    private static String getIPAdressByName(String var) {
        return apps_ip.get(var);
    }

    public static void send(DatagramSocket sock, DatagramPacket udpPacket, String host) throws UnknownHostException, IOException {
        String ipAdressByName = getIPAdressByName(host);
        InetAddress address = InetAddress.getByName(ipAdressByName);
        udpPacket.setAddress(address);
        sock.send(udpPacket);
    }
    
     public static synchronized boolean checkIfExists(String address) throws UnknownHostException {
        return peersList.contains(InetAddress.getByName(address));
    }
}
