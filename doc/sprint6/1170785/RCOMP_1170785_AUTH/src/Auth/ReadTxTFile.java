/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Auth;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author 1170785
 */
public class ReadTxTFile {

    public static UserAuth read() throws FileNotFoundException {
        File file = new File("persistent.txt");
        Scanner inputStream = new Scanner(file);
        UserAuth userA = new UserAuth();

        while (inputStream.hasNextLine()) {
            String[] info = inputStream.nextLine().split(";");
            if (info.length == 2) {
                userA.addUser(info[0], info[1].toUpperCase());
            }
        }
        inputStream.close();
        return userA;
    }

}
