/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Auth;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author 1170785
 */
public class HTTPServ implements Runnable {

    private final String BASE_FOLDER = "www/";
    private ServerSocket sock;
    private int port;

    public HTTPServ(int port) {
        this.port = port;
    }

    @Override
    public void run() {
        Socket cliSock;
        try {
            sock = new ServerSocket(port);
        } catch (IOException e) {
            System.out.println("Couldn't load local port " + port);
            System.exit(1);
        }
        while (true) {
            try {
                cliSock = sock.accept();
                HTTPReq req = new HTTPReq(cliSock, BASE_FOLDER);
                req.start();
            } catch (IOException ex) {
                System.out.println("Error on socket accept!");
            } catch (NoSuchAlgorithmException ex) {
            }
        }
    }
}
