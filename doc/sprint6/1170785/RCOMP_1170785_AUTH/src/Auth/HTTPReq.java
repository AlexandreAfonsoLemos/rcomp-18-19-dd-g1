/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Auth;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 1170785
 */
public class HTTPReq {

    private String baseFolder;
    private Socket sock;
    private DataInputStream inS;
    private DataOutputStream outS;

    public HTTPReq(Socket clientSock, String baseFolder) {
        this.sock = clientSock;
        this.baseFolder = baseFolder;
    }

    public void start() throws NoSuchAlgorithmException {
        try {
            outS = new DataOutputStream(sock.getOutputStream());
            inS = new DataInputStream(sock.getInputStream());
        } catch (IOException ex) {
            System.out.println("Thread error on data streams creation");
        }
        try {
            HTTPmessage request = new HTTPmessage(inS);
            HTTPmessage response = new HTTPmessage();
            response.setResponseStatus("200 ok");

            if (request.getMethod().equals("GET")) {
                if (request.getURI().startsWith("/walls/")) {
                    String[] splitUri = request.getURI().split("/");
                    String html = "";
                    response.setContentFromString(html, "text/html");
                } else {
                    String fullname = baseFolder;
                    if (request.getURI().equals("/")) {
                        fullname = fullname + "index.html";
                    } else {
                        fullname = fullname + request.getURI();
                    }
                    if (response.setContentFromFile(fullname)) {
                        response.setResponseStatus("200 Ok");
                    } else {
                        response.setContentFromString(
                                "<html><body><h1>404 File not found</h1></body></html>",
                                "text/html");
                        response.setResponseStatus("404 Not Found");
                    }
                }
                response.send(outS);
            } else if (request.getMethod().equals("PUT")) {
                if (request.getURI().startsWith("/auth/")) {
                    String[] splitUri = request.getURI().split("/");
                    UserAuth u = ReadTxTFile.read();
                    String authToken = u.getAuthToken(splitUri[3]);
                    String validationToken = validationToken = u.md5(u.md5(splitUri[4]).concat(authToken));
                    String authenticate = u.authenticate(splitUri[3], validationToken);
                    response.setContentFromString("Auth: " + authenticate, "text/html");
                } else {
                    response.setContentFromString(
                            "<html><body><h1>ERROR: 405 Method Not Allowed</h1></body></html>",
                            "text/html");
                    response.setResponseStatus("405 Method Not Allowed");
                }
                response.send(outS);
            }
        } catch (IOException ex) {
            System.out.println("Thread error when reading request");
        }
        try {
            sock.close();
        } catch (IOException ex) {
            System.out.println("CLOSE IOException");
        }
    }

}
