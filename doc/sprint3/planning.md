RCOMP 2018-2019 Project - Sprint 3 planning
===========================================
### Sprint master: 1170790 ###
(This file is to be created/edited by the sprint master only)
# 1. Sprint's backlog #

  * Development of a layer three Packet Tracer simulation for all buildings, including DHCP and VoIP service, and also encompassing the campus backbone. Integration of every members� Packet Tracer simulations into a single simulation.

# 2. Technical decisions and coordination #
  * IPv4 address space to be used: 172.18.32.0/19
  * Available IPv4 address: 8 190
  * ISP router�s IPv4 address: 190.5.200.65/30
  * Routers model: 2811
  
## 2.1. Assigned network addresses must meet the following requirements regarding the maximum number of nodes they will be able to support: 
  
### Building A and backbone 
  
  * End user outlets on the ground floor: 40 nodes
  * End user outlets on floor one: 40 nodes
  * Wi-Fi network: 24 nodes
  * Local servers and administration workstations (DMZ): 75 nodes
  * VoIP (IP-phones): 20 nodes
  * Backbone: 50 nodes
  * **IPv4 space address: 172.18.40.0/23**
  
### Building B
  
  * End user outlets on the ground floor: 60 nodes
  * End user outlets on floor one: 70 nodes
  * Wi-Fi network: 100 nodes
  * Local servers and administration workstations (DMZ): 12 nodes
  * VoIP (IP-phones): 30 nodes
  * **IPv4 space address: 172.18.42.0/23**
   
### Building C

  * End user outlets on the ground floor: 65 nodes 
  * End user outlets on floor one: 80 nodes 
  * Wi-Fi network: 120 nodes 
  * Local servers and administration workstations (DMZ): 12 nodes 
  * VoIP (IP-phones): 48 nodes
  * **IPv4 space address: 172.18.44.0/23**
  
### Building D 
  
  * End user outlets on the ground floor: 40 nodes 
  * End user outlets on floor one: 75 nodes 
  * Wi-Fi network: 140 nodes 
  * Local servers and administration workstations (DMZ): 12 nodes 
  * VoIP (IP-phones): 80 nodes
  * **IPv4 space address: 172.18.32.0/22**
  
### Building E

  * End user outlets on the ground floor: 45 nodes 
  * End user outlets on floor one: 80 nodes
  * Wi-Fi network: 150 nodes 
  * Local servers and administration workstations (DMZ): 12 nodes 
  * VoIP (IP-phones): 70 nodes
  * **IPv4 space address: 172.18.36.0/22**

### Building F 

  * End user outlets on the ground floor: 40 nodes
  * End user outlets on floor one: 90 nodes 
  * Wi-Fi network: 127 nodes
  * Local servers and administration workstations (DMZ): 12 nodes
  * VoIP (IP-phones): 67 nodes
  * **IPv4 space address: 172.18.46.0/23**
  
## The addresses block each team member will use:
  
| BUILDING | BACKBONE | GROUND FLOOR | FIRST FLOOR | VoIP | DMZ | WIFI |
| :------ | :--- | :--- | :--- | :--- | :--- | :--- |
| A | 172.18.40.192/26 | 172.18.40.64/26 | 172.18.40.128/26 | 172.18.40.0/27 | 172.18.41.0/25 | 172.18.40.32/27 |
| B | - | 172.18.42.128/25 | 172.18.43.0/25 | 172.18.42.64/26 | 172.18.42.0/26 | 172.18.43.128/25 |
| C | - | 172.18.44.128/25 | 172.18.45.0/25 | 172.18.44.64/26 | 172.18.44.0/26 | 172.18.45.128/25 |
| D | - | 172.18.32.128/25 | 172.18.33.0/24 | 172.18.34.0/24 | 172.18.32.0/25 | 172.18.35.0/24 |
| E | - | 172.18.36.128/25 | 172.18.37.128/25 | 172.18.37.0/25 | 172.18.36.0/25 | 172.18..38.0/23 |
| F | - | 172.18.46.64/26 | 172.18.47.0/25 | 172.18.46.128/25 | 172.18.46.0/26 | 172.18.47.128/25 |

## The IPv4 node address each router will use in the backbone network:

  * Router building A: 172.18.40.193/26
  * Router building B: 172.18.40.194/26
  * Router building C: 172.18.40.195/26
  * Router building D: 172.18.40.196/26
  * Router building E: 172.18.40.197/26
  * Router building F: 172.18.40.198/26
  
## VoIP phone numbers and prefix digits schema:

  * Phone numbers building A: 1000-1999
  * Phone numbers building B: 2000-2999
  * Phone numbers building C: 3000-3999
  * Phone numbers building D: 4000-4999
  * Phone numbers building E: 5000-5999
  * Phone numbers building F: 6000-6999
  * Phone numbers building G: 7000-7999

# 3. Subtasks assignment #
(For each team member (sprint master included), the description of the assigned subtask in sprint 3)

  * 1161071 - IPv4 addressing and routing configurations for building A
  * 1170785 - IPv4 addressing and routing configurations for building B. Integration of every members� Packet Tracer simulations into a single simulation.
  * 1170790 - IPv4 addressing and routing configurations for building C
  * 1170508 - IPv4 addressing and routing configurations for building D
  * 1170505 - IPv4 addressing and routing configurations for building E
  * 1170803 - IPv4 addressing and routing configurations for building F