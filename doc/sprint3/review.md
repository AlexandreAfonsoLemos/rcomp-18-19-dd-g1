RCOMP 2018-2019 Project - Sprint 3 review
=========================================
### Sprint master: 1170790 ###
(This file is to be created/edited by the sprint master only)
# 1. Sprint's backlog #
 * Development of a layer three Packet Tracer simulation for all buildings, including DHCP and VoIP service, and also encompassing the campus backbone. Integration of every members’ Packet Tracer simulations into a single simulation.
# 2. Subtasks assessment #
One by one, each team member presents his/her outcomes to the team, the team assesses the accomplishment of the subtask backlog.
The subtask backlog accomplishment can be assessed as one of:

  * Totally implemented with no issues
  * Totally implemented with issues
  * Partially implemented with no issues
  * Partially implemented with issues

For the last three cases, a text description of what has not been implemented and present issues must be added.
Unimplemented features and issues solving is assigned to the same member on the next sprint.

## 2.1. 1161071 - IPv4 addressing and routing configurations for building A #
### Partily implemented with no issues. ###
DHCP for PC atached to the phone is not reaching PC. Switchport only support Voice VLAN and no access is specified.
Phone routing are not specified.
DHCP for DMZ should not be specified.
## 2.2. 1170785 - IPv4 addressing and routing configurations for building B #
### Partily implemented with no issues. ###
DHCP for PC atached to the phone is not reaching PC. Switchport only support Voice VLAN and no access is specified.
Phone routing are not specified.
DHCP for DMZ should not be specified.
## 2.3. 1170790 -IPv4 addressing and routing configurations for building C #
### Partily implemented with no issues. ###
DHCP for PC atached to the phone is not reaching PC. Switchport only support Voice VLAN and no access is specified.
Phone routing are not specified.
DHCP for DMZ should not be specified.
## 2.4. 1170508 - IPv4 addressing and routing configurations for building D #
### Partily implemented with no issues. ###
Phone routing are not specified.
DHCP for DMZ should not be specified.
## 2.5. 1170505 - IPv4 addressing and routing configurations for building E #
### Partily implemented with no issues. ###
Phone routing are not specified.
DHCP for DMZ should not be specified.
## 2.6. 1170803 - IPv4 addressing and routing configurations for building F #
### Partily implemented with no issues. ###
Phone routing are not specified.
DHCP for DMZ should not be specified.
