RCOMP 2018-2019 Project - Sprint 3 - Member 1170508 folder
===========================================
(This folder is to be created/edited by the team member 1170508 only)

The owner of this folder (member number 1170508) will commit here all the outcomes (results/artifacts/products) of his/her work during sprint 3. This may encompass any kind of standard file types.


# Atribuicao de endereços IPv4

#### - Para o edificio D o bloco de endereços IPv4 é: 172.18.32/22

# Tabela Sub-Interfaces

- As sub-interfaces criadas seguem a seguinte regra f0/0.VLAN, sendo VLAN a vlan a qual querem atribuir a sub-interface.

| ip address | Nome |Sub-interface |  
| :------- | :------- |:------- |
| 172.18.32.254/25 | GroundFloor | f0/0.174 |
| 172.18.33.254/24 | FirstFloor |f0/0.184|
| 172.18.35.254/24 | Wifi |f0/0.194|
| 172.18.34.254/24  | VoIPPhone |f0/0.199|
| 172.18.32.126/25 | DMZ |f0/0.204|
| 172.18.40.196/26 | Backbone |f0/0.210||
