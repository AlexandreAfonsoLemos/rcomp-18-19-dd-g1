!
version 12.4
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Router_B
!
!
!
!
!
ip dhcp pool VLAN172
 network 172.18.42.128 255.255.255.128
 default-router 172.18.42.254
ip dhcp pool VLAN182
 network 172.18.43.0 255.255.255.128
 default-router 172.18.43.126
ip dhcp pool VLAN208
 network 172.18.42.64 255.255.255.192
 default-router 172.18.42.126
 option 150 ip 172.18.42.126
ip dhcp pool VLAN202
 network 172.18.42.0 255.255.255.192
 default-router 172.18.42.62
ip dhcp pool VLAN192
 network 172.18.43.128 255.255.255.128
 default-router 172.18.43.254
ip dhcp pool VLAN210
 network 172.18.40.192 255.255.255.192
 default-router 172.18.40.194
!
!
!
ip cef
no ipv6 cef
!
!
!
!
!
!
!
!
!
!
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface FastEthernet0/0
 no ip address
 duplex auto
 speed auto
!
interface FastEthernet0/0.172
 encapsulation dot1Q 172
 ip address 172.18.42.254 255.255.255.128
!
interface FastEthernet0/0.182
 encapsulation dot1Q 182
 ip address 172.18.43.126 255.255.255.128
!
interface FastEthernet0/0.192
 encapsulation dot1Q 192
 ip address 172.18.43.254 255.255.255.128
!
interface FastEthernet0/0.202
 encapsulation dot1Q 202
 ip address 172.18.42.62 255.255.255.192
!
interface FastEthernet0/0.208
 encapsulation dot1Q 208
 ip address 172.18.42.126 255.255.255.192
!
interface FastEthernet0/0.210
 encapsulation dot1Q 210
 ip address 172.18.40.194 255.255.255.192
!
interface FastEthernet0/1
 no ip address
 duplex auto
 speed auto
 shutdown
!
interface Vlan1
 no ip address
 shutdown
!
ip classless
ip route 172.18.46.64 255.255.255.192 172.18.40.198 
ip route 172.18.47.0 255.255.255.128 172.18.40.198 
ip route 172.18.46.128 255.255.255.128 172.18.40.198 
ip route 172.18.46.0 255.255.255.192 172.18.40.198 
ip route 172.18.47.128 255.255.255.128 172.18.40.198 
ip route 172.18.44.128 255.255.255.128 172.18.40.195 
ip route 172.18.45.0 255.255.255.128 172.18.40.195 
ip route 172.18.44.64 255.255.255.192 172.18.40.195 
ip route 172.18.45.128 255.255.255.128 172.18.40.195 
ip route 172.18.44.0 255.255.255.192 172.18.40.195 
ip route 172.18.40.64 255.255.255.192 172.18.40.193 
ip route 172.18.40.128 255.255.255.192 172.18.40.193 
ip route 172.18.40.0 255.255.255.224 172.18.40.193 
ip route 172.18.41.0 255.255.255.128 172.18.40.193 
ip route 172.18.40.32 255.255.255.224 172.18.40.193 
ip route 172.18.32.128 255.255.255.128 172.18.40.196 
ip route 172.18.33.0 255.255.255.0 172.18.40.196 
ip route 172.18.34.0 255.255.255.0 172.18.40.196 
ip route 172.18.32.0 255.255.255.128 172.18.40.196 
ip route 172.18.35.0 255.255.255.0 172.18.40.196 
ip route 172.18.36.128 255.255.255.128 172.18.40.197 
ip route 172.18.37.128 255.255.255.128 172.18.40.197 
ip route 172.18.37.0 255.255.255.128 172.18.40.197 
ip route 172.18.36.0 255.255.255.128 172.18.40.197 
ip route 172.18.38.0 255.255.254.0 172.18.40.197 
!
ip flow-export version 9
!
!
!
!
!
!
!
!
dial-peer voice 188 voip
 destination-pattern 3...
 session target ipv4:172.18.44.126
!
dial-peer voice 197 voip
 destination-pattern 6...
 session target ipv4:172.18.46.254
!
dial-peer voice 199 voip
 destination-pattern 4...
 session target ipv4:172.18.34.254
!
dial-peer voice 207 voip
 destination-pattern 5...
 session target ipv4:172.18.37.126
!
dial-peer voice 209 voip
 destination-pattern 1...
 session target ipv4:172.18.40.30
!
line con 0
!
line aux 0
!
line vty 0 4
 login
!
!
!
end

