RCOMP 2018-2019 Project - Sprint 3 - Member 1170505 folder
===========================================
(This folder is to be created/edited by the team member 1170505 only)

The owner of this folder (member number 1170505) will commit here all the outcomes (results/artifacts/products) of his/her work during sprint 3. This may encompass any kind of standard file types.


### DHCP (Dynamic Host Configuration Protocol) service configuration

- Ao usar o DHCP, quando um node é conectado a uma rede desconhecida(se o Servidor DHCP estiver a correr), todos os dados de configuração serão atribuidos.
- O DHCP pode atribuir endereços estáticos, o que significa que um node tem sempre o mesmo endereço fixo.

### Sub-Interfaces | atribuição de IPs

 - Apos a colocação dos routers, foram criadas sub-Interfaces para cada VLAN, e atribuidos os IPs.
 - Para cada sub-interface foi necessário atribuir um IP, correspondente ao sub-bloco definido em cima, mediante a VLAN a que essa sub-interface estava atribuida. De acordo com o pré-determinado pela equipa, este IP seria sempre o último disponível do bloco.
- Por Exemplo:
	1. f0/0.195 (VLAN 195 - Piso 0)

### Mascara de rede

  - O endereço de rede é determinado pela máscara de rede. Ambos são um número de 32 bits.
  - Representados por sequências onde 1 byte corresponde aos bits mais significativos usados para representar a rede, seguidos por bits com valor zero, que representam quantos IP aquela rede dispõe.
  - Para representar um endereço IPv4 é necessário o endereço de rede e a sua respetiva máscara.
  - Os blocos foram atribuidos com a atenção de nenhum se sobrepôr a outro (overlaping).

### Routing dinâmico

- Uma infra-estrutura de rede IPv4 pode ser redundante, isso significa que existem várias maneiras para alcançar a mesma rede de destino.
- Com uma infra-estrutura estática, o routing dinâmico torna se uma necessidade.
- O  routing dinâmico impõe um equilibrio de carga efetivo quando existem várias alternativas. Cada router seleciona sempre a alternativa de menor custo que encontra na tabela de routing, mas os valores de custo nas tabelas de routing são permanentemente atualizados pelo protocolo.

### Endereço de rede

- Um endereço de rede é o identificador exclusivo de uma rede. Cada rede na camada 2 precisam de um endereço de rede diferente na camada 3 pois:
	- Os routers só reenviam IP packets entre redes com diferentes endereços.

	- Quando um node está a enviar um packet, ele deve saber se o endereço de destino é na sua própria rede.
  Se sim, o IP packet pode ser enviado diretamente usando a camada 2, caso contrário, o IP packet deve ser enviado para um router.

  - Na camada três, qualquer endereço de um node deve incluir em si o endereço de rede. Sendo que, os bits mais significativos de um IP vão sempre identificar a rede a que este pertence, e os restantes individualizam esse node dentro da rede.
