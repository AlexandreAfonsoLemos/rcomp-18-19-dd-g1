!
version 12.4
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Router
!
!
!
!
!
ip dhcp pool VLAN176
 network 172.18.46.64 255.255.255.192
 default-router 172.18.46.126
ip dhcp pool VLAN186
 network 172.18.47.0 255.255.255.128
 default-router 172.18.47.126
ip dhcp pool VLAN197
 network 172.18.46.128 255.255.255.128
 default-router 172.18.46.254
 option 150 ip 172.18.46.254
ip dhcp pool VLAN206
 network 172.18.46.0 255.255.255.192
 default-router 172.18.46.62
ip dhcp pool VLAN196
 network 172.18.47.128 255.255.255.128
 default-router 172.18.47.254
ip dhcp pool VLAN210
 network 172.18.40.192 255.255.255.192
 default-router 172.18.40.198
!
!
!
ip cef
no ipv6 cef
!
!
!
!
!
!
!
!
!
!
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface FastEthernet0/0
 no ip address
 duplex auto
 speed auto
!
interface FastEthernet0/0.176
 encapsulation dot1Q 176
 ip address 172.18.46.126 255.255.255.192
!
interface FastEthernet0/0.186
 encapsulation dot1Q 186
 ip address 172.18.47.126 255.255.255.128
!
interface FastEthernet0/0.196
 encapsulation dot1Q 196
 ip address 172.18.47.254 255.255.255.128
!
interface FastEthernet0/0.197
 encapsulation dot1Q 197
 ip address 172.18.46.254 255.255.255.128
!
interface FastEthernet0/0.206
 encapsulation dot1Q 206
 ip address 172.18.46.62 255.255.255.192
!
interface FastEthernet0/0.210
 encapsulation dot1Q 210
 ip address 172.18.40.198 255.255.255.192
!
interface FastEthernet0/1
 no ip address
 duplex auto
 speed auto
 shutdown
!
interface Vlan1
 no ip address
 shutdown
!
ip classless
ip route 172.18.42.128 255.255.255.128 172.18.40.194 
ip route 172.18.43.0 255.255.255.128 172.18.40.194 
ip route 172.18.43.128 255.255.255.128 172.18.40.194 
ip route 172.18.42.0 255.255.255.192 172.18.40.194 
ip route 172.18.42.64 255.255.255.192 172.18.40.194 
!
ip flow-export version 9
!
!
!
!
!
!
!
!
line con 0
!
line aux 0
!
line vty 0 4
 login
!
!
!
end

