RCOMP 2018-2019 Project - Sprint 3 - Member 1170785 folder
===========================================
(This folder is to be created/edited by the team member 1170785 only)

The owner of this folder (member number 1170785) will commit here all the outcomes (results/artifacts/products) of his/her work during sprint 3. This may encompass any kind of standard file types.

# Explicação Adicional

* Este sprint teve como objetivo a atribuição de endereços de IP, utilizando o serviço Dynamic Host Configuration Protocol (DHCP), e implementando protocolos de routing dinâmico. 

## Atribuição dos endereços IPv4

### Endereço de rede

* Um endereço de rede é um identificador exclusivo de uma rede. Cada rede na layer 2 precisam de um endereço de rede diferente na layer 3, isso é crucial, porque:

	* Os routers só retransmitem packets de IP entre redes com diferentes endereços (caso contrário não são redes diferentes).
		
	* Quando um node está enviando um packet, ele deve saber se o endereço de destino é na sua própria rede, em caso afirmativo, o packet de IP pode ser enviado diretamente usando a layer 2, caso contrário, o packet IP deve ser enviado para um router.
		
* Na camada três, qualquer endereço de um node deve incluir em si o endereço de rede, ou seja, o node pertence a essa rede. Sendo que, os bits mais significativos de um IP vão sempre identificar a rede a que este pertence, e os restantes individualizam esse node dentro da rede.
 
### Mascara de rede

 * O endereço de rede é determinado pela máscara de rede. Uma máscara de rede também é número de 32 bits, mas é sempre feito de uma sequência em 1 bits correspondente aos bits mais significativos usados para representar a rede, seguidos poros bits restantes com valor zero, que representam quantos IP aquela rede dispõe.
 
 * Para representar um endereço IPv4 é necessário o endereço de rede mais a sua máscara. Relativamente ao número de endereços IPv4 uma dada rede suporta, com uma determinada máscara é calculada pela seguinte expressão: (2^32-N)- 2, em que N é o prefixo da mascara, e a subtração de 2 porque o primeiro corresponde ao endereço de rede e o último ao endereço do broadcast.
 * Para o edificio B foi delimitado um bloco de IPv4 endereços: 172.18.42.0/23.
 * Seguindo os protocolos de atribuição lecionados, os sub-blocos foram divididos da seguinte forma:
 
	1. DMZ (12 nodes): 172.18.42.0/26
	2. VoIP (30 nodes): 172.18.42.64/26
	3. Piso 0 (60 nodes): 172.18.42.128/25
	4. Piso 1(70 nodes): 172.18.43.0/25
	5. Wifi (100 nodes): 172.18.43.128/25
	
 * Foi aribuido cada um deste seguintes blocos com a atenção de nenhum se sobrepôr a outro (overlaping).
 
## Sub-Interfaces e atribuição de IPs

 * Com cada router já colocado, procedeu-se à criação de sub-Interfaces (derivadas da interface FastEthernet0/0) para cada VLAN, e atribuição de IPs.
 * Foi usado o padrão IEEE 802.1Q, com o propósito de instruir os nodes a trocarem entre si, apenas pacjets contendo um mesmo identificador, e assim, atenuar os loops de informação (Spanning-Tree Protocol).
 * Para cada sub-interface foi necessário atribuir um IP, correspondente ao sub-bloco definido em cima, mediante a VLAN a que essa sub-interface estava atribuida. De acordo com o pré-determinado pela equipa, este IP seria sempre o último disponível do bloco.
 
	1. f0/0.172 (VLAN 172 - Piso 0): 172.18.42.254/25
	2. f0/0.182 (VLAN 182 - Piso 1): 172.18.43.126/25
	3. f0/0.192 (VLAN 192 - Wifi): 172.18.43.254/25
	4. f0/0.202 (VLAN 202 - DMZ): 172.18.42.62/26
	5. f0/0.208 (VLAN 208 - VoIP): 172.18.42.126/26
	6. f0/0.210 (VLAN 210 - Backbone): 172.18.40.194/26 (já foi atribuida no planning.md)
	
## DHCP service configuration

* Desejavelmente nodes não são obrigados a saber de antecipadamente a configuração de rede IPv4 que eles deveriam usar, que inclui o endereço do node IPv4 local, a máscara de rede e orouter da rede (gateway default).
* Mesmo que, todos esses dados de configuração IPv4 são necessários para o node operar, estes podem ser recuperado usando protocolos específicos de configuração automática, especialmente DHCP. Usando o DHCP, quando um node é conectado a uma rede desconhecida, se houver Servidor DHCP em execução, todos os dados de configuração necessários serão fornecidos. Além da configuração já referida, isso geralmente também abrange
* O DHCP pode ser usado para atribuir endereços estáticos, o que significa que um node sempre têm o mesmo endereço fixo, mas para nós de usuário final que não é relevante.
* Geralmente o servidor DHCP gere um bloco de endereços de nodes IPv4 (pool de endereços) e designa-os livremente conforme necessário.
* Só foi utilizados endereços dinâmicos para as VLANs 172, 182, 192 e 202, ou seja, não foi utilizado para os DMZ ou Backbone.

## Routing dinâmico

* Uma infra-estrutura de rede IPv4 pode ser redundante, que é o caso deste Campus, isso significa que vários caminhos para alcançar a mesma rede de destino.
* Com uma infra-estrutura estática, aquando um malfuncionamento de um dos aparelhos, o administrador da rede teria que se apressar e ir para cada router removendo todas as rotas que passam pelo dispositivo com falha. Aqui é onde o routing dinâmico se torna uma obrigação.
* O protocolo de routing estabelece um diálogo entre routers, levando-os a cooperar e construir automaticamente uma tabela de routing em todos eles. Além disso elas são permanentemente atualizadas para refletir o status da rede.
* Além disso, o routing dinâmico impõe um equilibrio de carga efetivo quando existem várias alternativas. Cada router seleciona sempre a alternativa de menor custo que encontra na tabela de routing, mas os valores de custo nas tabelas de routing são permanentemente atualizado pelo protocolo. Por exemplo, se um router estiver muito carregado com o tráfego, o protocolo aumenta o custo desse caminho em todas as tabelas. Além disso, se um router não estiver disponível, o protocolo remove a alternativa de todas as tabelas.
