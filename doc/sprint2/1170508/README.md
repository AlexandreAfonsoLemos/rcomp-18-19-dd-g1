RCOMP 2018-2019 Project - Sprint 2 - Member 1170508 folder
===========================================

# Notas Packet Tracer

- As ligações que estão representadas por cabo de cobre cross-over, são de fibra.

- Para a redundância utilizamos a metodologia em V.

# VLAN´s

| NUMBER | NAME |
| :------ | :--- |
|171 | A_GROUNFLOOR|
|172 | B_GROUNFLOOR|
|173 | C_GROUNFLOOR|
|174 | D_GROUNFLOOR|
|175 | E_GROUNFLOOR|
|176 | F_GROUNFLOOR|
|181 | A_FIRSTFLOOR|
|182 | B_FIRSTFLOOR|
|183 | C_FIRSTFLOOR|
|184 | D_FIRSTFLOOR|
|185 | E_FIRSTFLOOR|
|186 | F_FIRSTFLOOR|
|191 | A_NETWORK   |
|192 | B_NETWORK   |
|193 | C_NETWORK   |
|194 | D_NETWORK   |
|195 | E_NETWORK   |
|196 | F_NETWORK   |
|201 | A_ADMIN     |
|202 | B_ADMIN     |
|203 | C_ADMIN     |
|204 | D_ADMIN     |
|205 | E_ADMIN     |
|206 | F_ADMIN     |
|199 | D_PHONE     |
|210 | MAIN |
