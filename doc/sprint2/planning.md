RCOMP 2018-2019 Project - Sprint 2 planning
===========================================
### Sprint master: 1161071 ###

# 1. Sprint's backlog #
Development of a layer two Packet Tracer simulation for building A, and also encompassing the campus backbone. Integration of every members' Packet Tracer simulations into a single simulation.
Development of a layer two Packet Tracer simulation for building B, and also encompassing the campus backbone.
Development of a layer two Packet Tracer simulation for building C, and also encompassing the campus backbone.
Development of a layer two Packet Tracer simulation for building D, and also encompassing the campus backbone.
Development of a layer two Packet Tracer simulation for building E, and also encompassing the campus backbone.
Development of a layer two Packet Tracer simulation for building G, and also encompassing the campus backbone.
Development of a layer two Packet Tracer simulation for building F, and also encompassing the campus backbone.

# 2. Technical decisions and coordination #
Regras de nomenclatura:

	- Novos Devices:
		-> TipoDeDevice_LetraDoEdificio_Piso_Sala*_numero
			*se for um despositivo que tenha que estar dentro de alguma sala em particular vai dar jeito qd for para montar tudo
			EX: PC_C_0_11_1

	-Devices de outros edificios: 
		Contexto: "Each of these simulations will encompass a single building, however, they must include the campus backbone, meaning they 
will include a switch representing the main cross-connect, and switches representing each intermediate cross-connect in other buildings. "
		-> não acho que a nomenclatura deva mudar
			EX: SWITCH_A_1_1_1

	
	- VLANs (31 VLANs no total):
		- One VLAN for all end-user outlets on the ground floor.
			VLANID 17*, nome/hostname: **_GROUDFLOOR

		- One VLAN for all end-user outlets on floor one of the building. 
			VLANID 18*, nome/hostname: **_FIRSTFLOOR

		- One VLAN for the Wi-Fi network (all access-points’ outlets within the building).
			VLANID 19*, nome/hostname: **_NETWORK

		- One VLAN for local servers and administration workstations (DMZ). 
			VLANID 20*, nome/hostname: **_ADMIN

		- One VLAN for VoIP (IP-phones).
			VLANID ***, nome/hostname: **_PHONE 

		- One VLAN in common for all buildings
			VLANID 210, nome/hostname: MAIN

*Aqui é diferente para cada edificio e eu pensei em pormos a posição da letra no alfabeto --> A = 1 entao fica 171/181/191/201...
											      B = 2 entao fica 172/182/192/202/...
**Aqui é diferente para cada edificio e eu pensei em pormos a LETRA do nosso edificio.

***Aqui podemos escolher cada um, um numero destes a seguir (177/178/179/187/188/189/197/198/199/207/208/209) 

foi usada fibra no backbone da estrutura pela velocidade de transferencia de informação e por não ser afetado por ruidos.

Os canais utilizados no wifi foram o 1 6 e 11 para não existir perda de sinal ao mudar de sinal de um access para o outro , dois access points em serie não usam o mesmo canal.

Foi utilizado redundancia de cabos e equipamento , para não perder sinal no caso de falha de um deles.

Entre dois edificios quando é feita redundancia os cabos sao direncionados em caminhos diferentes para evitar a perda de ambos em caso de um acidente local

# 3. Subtasks assignment #
(For each team member (sprint master included), the description of the assigned subtask in sprint 2)

1170505 - Layer two switching for building E.

1170508 - Layer two switching for building D.

1170785 - Layer two switching for building B.

1170790 - Layer two switching for building C.

1161071 - Layer two switching for building A. Integration of all the buildingins.

1170803 - Layer two switching for building E.
