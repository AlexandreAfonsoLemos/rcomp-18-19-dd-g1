RCOMP 2018-2019 Project - Sprint 2 - Member 1170505 folder
===========================================
(This folder is to be created/edited by the team member 1170505 only)

The owner of this folder (member number 1170505) will commit here all the outcomes (results/artifacts/products) of his/her work during sprint 2. This may encompass any kind of standard file types.

# Notas Packet Tracer

- As ligações que estão representadas por cabos cross-over de cobre são de fibra.

- O número de switchs não está de acordo com o sprint 1 para simplificação do projeto e para não sobrecarregar o packet tracer.

## Informação relacionada com o Sprint

- Ligações entre dispositivos iguais são configuradas como Trunk mode e são escolhidas as VLANs que são permitidas serem partilhadas.

- Ligações entre dispositivos diferentes são configuradas como Access mode e apenas uma VLAN é permitida.

- Uma Virtual Local Area Network (VLAN) é parte de uma infraestrutura de 2 camadas com o objetivo de agir como uma network de duas camadas separada. Uma frame de duas camadas enviada por uma VLAN nunca vai alcançar outra VLAN ou partes da infraestrutura não atribuídas a nenhuma VLAN, logo uma VLAN e um domínio de broadcast único e distinto, e age como uma LAN.

- Uma VLAN deve ser equivalente a uma LAN distinta.

- Um dispositivo deve ser escolhido para conter a master copy da VLAN database, e é neste dispositivo que o administrador deve definir e alterar os dados das VLANs quando necessário.
Este dispositivo encontra se no VTP Server mode.
- Outros dispositivos apenas recebem a VLAN database do VTP Server(do mesmo domínio).
Estão configurados como VTP Client mode.
