RCOMP 2018-2019 Project - Sprint 2 - Member 1170785 folder
===========================================
(This folder is to be created/edited by the team member 1170785 only)

The owner of this folder (member number 1170785) will commit here all the outcomes (results/artifacts/products) of his/her work during sprint 2. This may encompass any kind of standard file types.

# Explicação Adicional

 * Neste sprint, o objetivo foi a layer 2 (Switches e Access Points), mas também alguns dispositivos finais serão adicionados em preparação para o próximo sprint. O foco é criar um layout de rede lógico e não físico, que, no entanto, deve ser representativo do projeto de cabeamento estruturado (sprint anterior):
 	
	1. Cabeamento Redundante foi escolhido para ser representado por dois "caminhos" alternativos a ligar dois dispositivos (usado Spanning Tree Protocol).
	2. Os tipos de cabo utilizados anteriormente no projeto, foram mantidos excepto entre o MCC e o ICC que por razões do ultrapasso da capacidade de portas de fibra foi escolhido representar por cobre.
	3. O modelo do switch que usado foi o PT-empty com portas do tipo FastEthernet ou GigaEthernet.
	4. Foram criadas e programadas VLANs segundo as diretrizes do planning.md.
	5. VLAN Truncking Protocol (VTP) foi utilizado para segurar a mesma BD de VLANs.
	6. Para o edificio pelo menos um dispositivo final por VLAN foi adicionado.
	
## Estrutura lógica

 * Foram usados dois switches para representar o MCC com ligações e modo trunk e com cross-cable, visto que estão ambos ligados a outros switches.
 * Para representar o ICC, em todos os edificios exceto o B foram representados apenas um switch. No caso do B, foram colocados 2 para garantir a redundancia.
 * Em relação aos HCC e CP, estes foram reprentados também por switches ligados a ambos ICCs. Neste caso usou-se fibra.
 * Dos HCC e CP aos outlets (agora representados por dispositivos finais como PCs, Access Points, Laptops, DMZ e IP-phones, foi usado cobre.
 
| HARDWARE | QUANTIDADE | UNIDADE/PISO | 
| :---- | :---- | :---- |
| PC | 2 | 1/piso |
| PC + IP-phone | 1 | 1 no piso 1 |
| SERVER | 1 | 1 no piso 0 |
| LAPTOP | 1 | 1 no piso 0 |
| ACCESS POINT | 4 | 2/piso |
| SWITCH | 11 | - | 
	
## Criação e Configuração das VLANs

 * Uma rede local virtual (VLAN) faz parte da layer 2, sendo assim, uma infra-estrutura destinada a funcionar como uma camada separada de duas redes. Ou seja, uma VLAN é única tendo um domínio de transmissão distinto de qualquer outra, comportando-se como uma LAN.
 * As VLANs do edificio C são:
 	
| ID | NOME | DESCRIÇÃO |
| :---- | :---- | :---- |
| 172 | B_GROUNDFLOOR | Pertencem a esta todos os outlets finais do piso 0 |
| 182 | B_FIRSTFLOOR | Pertencem a esta todos os outlets finais do piso 1 |
| 208 | B_PHONE | Pertencem a esta todos os telefones do edificio |
| 192 | B_WIFI | Pertencem a esta todos os outlets ligados a uma rede Wifi |
| 202 | B_ADMIN | Pertencem a esta todos servidores locais e estações de trabalho de administração de todo o edificio |

## VTP – VLAN Trunking Protocol

 * O VTP é um protocolo proprietário muito simples da CISCO com o objetivo de manter atualizado a base de dados de VLAN em todos os dispositivos. Todos os dispositivos que devem compartilhar a mesma base de dados devem pertencer o mesmo domínio VTP (um nome de 1 a 32 caracteres). 
 * Um dispositivo deve ser selecionado para manter a cópia principal do banco de dados da VLAN, é neste dispositivo que o administrador deve definir e alterar a base de dados quando requisitado. Este dispositivo será configurado no modo de servidor VTP (**server mode**). 
 * Outros dispositivos atuarão como escravos e receberão a base de dados VLAN do Servidor VTP (do mesmo domínio VTP). Eles serão configurados no cliente VTP (**client mode**).
 * VTP só opera por ligações em modo trunck (mais do que uma VLAN é passada nessa ligação), equanto que na ligação direta com os dispositivos finais foi usada o modo access (apenas uma VLAN é passada nesta ligação) exceto no conjunto PC + IP-phone.
 * No projeto foi atribuido o nome **vtddg1** ao domínio VTP.
