RCOMP 2018-2019 Project repository 2DD-G1
===========================================
# 1. Team members  #
  * 1170505 - {Alexandre Afonso} 
  * 1170508 - {Murilo Couceiro} 
  * 1170785 - {Ana Mata} 
  * 1170790 - {Daniel Dias} 
  * 1161071 - {Pedro Miura} 
  * 1170803 - {Francisco Teixeira} 

Any team membership changes should be reported here, examples:

Member 1111111 ({First and last name}) has left the team on 2019-03-20

Member 2222222 ({First and last name}) has entered the team on 2019-04-5

# 2. Sprints #
  * [Sprint 1](doc/sprint1/)
  * [Sprint 2](doc/sprint2/)
  * [Sprint 3](doc/sprint3/)
  * [Sprint 4](doc/sprint4/)
  * [Sprint 5](doc/sprint5/)
  * [Sprint 6](doc/sprint6/)
